﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;

namespace ConsoleApplication1
{
    class TestStorage1
    {
            public CloudBlobContainer GetCloudBlobContainer(string name)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    ConfigurationManager.ConnectionStrings["StorageConnectionString2"].ConnectionString//..AppSettings["StorageConnectionString"]
                    );
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(name);
               // blobContainer.
                if (blobContainer.CreateIfNotExists())
                {
                    blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }
                return blobContainer;
            }

            public IEnumerable<IListBlobItem> GetDirectoryList(string directoryName, string subDirectoryName)
            {
                CloudStorageAccount account =
                    CloudStorageAccount.Parse(
                    ConfigurationManager.ConnectionStrings["StorageConnectionString2"].ConnectionString//..AppSettings["StorageConnectionString"]
                    );
                CloudBlobClient client = account.CreateCloudBlobClient();
                //CloudBlobDirectory directory =cloudBlobClient.GetBlobDirectoryReference(directoryName);
                //CloudBlobDirectory subDirectory =directory.GetSubdirectory(subDirectoryName);
                CloudBlobContainer d1 = client.GetContainerReference("upload");
                CloudBlobDirectory di2 = d1.GetDirectoryReference("avatar");
                //d1CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("upload");
                    CloudBlockBlob blob = d1.GetBlockBlobReference("avatar/user2009608.jpg");
                    //d1.ListBlobs(new BlobRequestOptions()
                    //{
                    //    UseFlatBlobListing = true
                    //});  
                    List<string> tt = new List<string>();
                foreach (IListBlobItem item in di2.ListBlobs(true,BlobListingDetails.None))
                {
                    tt.Add(item.Uri.LocalPath);
                }
                //lobRequestOptions options = new BlobRequestOptions();
                //options.UseFlatBlobListing = true;
               List<IListBlobItem> blobs = di2.ListBlobs(true,BlobListingDetails.None).ToList();
                var gg = (from r in tt
                          where r.Equals("upload/avatar/1209068_579457558778794_2000967271_n.jpg")//upload/avatar/User2007603.jpg")
                             select r).ToList();
                //CloudBlobDirectory di3 = di2.GetSubdirectoryReference("avatar");
                var fg = (from r in tt
                          where r.Equals(directoryName)
                          select r);
                var kk = gg.AsEnumerable();
                return blobs;//.ListBlobs();
            }
        }
    }
