﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuikSiteWalkModel1;
using BuikSiteWalkModel1.Services;

namespace DemoBuikAlp1.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class ErrorHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        private void HandleGetFail(ExceptionContext exceptionContext, UrlHelper urlHelper, string errorMsg)
        {
            var request = exceptionContext.RequestContext.HttpContext.Request;
            if (request["X-Requested-With"] == "XMLHttpRequest" || request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                exceptionContext.Result = new JsonResult
                {
                    Data = new { success = false, error = "There's somthing wrong with server. Please try again later." },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }

            //Old version
            var result = new ViewResult();
            result.ViewData["Error"] = errorMsg ?? "There's something wrong.";
            result.ViewName = "Error";
            exceptionContext.Result = result;
            exceptionContext.Controller.TempData.Clear();

        }

        /// <summary>
        /// ควบคุมการตอบกลับกรณีเป็น action post
        /// </summary>
        /// <param name="exceptionContext"></param>
        /// <param name="urlHelper"></param>
        /// <param name="errorMsg"></param>
        private void HandlePostFail(ExceptionContext exceptionContext, UrlHelper urlHelper, string errorMsg)
        {
            var request = exceptionContext.RequestContext.HttpContext.Request;
            if (request["X-Requested-With"] == "XMLHttpRequest" || request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                exceptionContext.Result = new JsonResult
                {
                    Data = new { success = false, error = "There's somthing wrong with server. Please try again later." },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }
            else
            {
                //handler POST fail
                RedirectResult result;
                if (exceptionContext.HttpContext.Request.UrlReferrer != null)
                    result =
                        new RedirectResult(
                            urlHelper.Content(
                                exceptionContext.HttpContext.Request.UrlReferrer.PathAndQuery));
                else
                {
                    var sessionHandler = new UserSession(exceptionContext.HttpContext);

                    if (sessionHandler.UserInfoKeySurrKey == 0)
                    {
                        result = new RedirectResult(urlHelper.Content("~/account/login") + "?ReturnUrl=" + exceptionContext.HttpContext.Request.Url.AbsoluteUri);
                    }
                    else
                    {
                        result = new RedirectResult((urlHelper.Content("~/mybuilk/wall")));
                    }
                }
                exceptionContext.Result = result;
                exceptionContext.Controller.TempData["Error"] = errorMsg;
            }
            //if (string.IsNullOrWhiteSpace(errorMsg))
            //    exceptionContext.Controller.TempData["ErrorMessage"] = new List<string> { App_GlobalResources.ErrorMessages.Some };
            //else
            //    exceptionContext.Controller.TempData["ErrorMessage"] = new List<string> { errorMsg };
        }

        public void OnException(ExceptionContext exceptionContext)
        {
            exceptionContext.ExceptionHandled = true;
            var urlRoute = new UrlHelper(exceptionContext.RequestContext);

            // Preparing For Loging
            var sessionHandler = new UserSession(exceptionContext.HttpContext);
            var userId = sessionHandler.UserInfoKeySurrKey;
            //var userName = "";
            //var errorLogService = new ErrorLogService(new DataLayerContext());

            // check: Is there some exception?
            if (exceptionContext.Exception != null)
            {
                switch (exceptionContext.Exception.GetType().Name)
                {
                    case "SessionLostException":
                        exceptionContext.RequestContext.HttpContext.Response.StatusCode = 401;
                        var request = exceptionContext.RequestContext.HttpContext.Request;
                        if (request["X-Requested-With"] == "XMLHttpRequest" || request.Headers["X-Requested-With"] == "XMLHttpRequest")
                        {
                            exceptionContext.Result = new JsonResult
                            {
                                Data = new { success = false, error = "enable login dialog" },
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                        else
                        {
                            exceptionContext.Result = new RedirectResult(urlRoute.Content("https://app.builk.com/mybuilk/wall"));
                        }
                        break;
                    default:
                        IMailService MailService = new MailService();
                        
                        Boolean willSendErrorReport = true;
                        
                        #if DEBUG
                            willSendErrorReport = false;
                        #endif

                        //Send email only if on production server
                        if (willSendErrorReport)
                        {
                            MailService.SendErrorReport(exceptionContext);
                        }

                        if (exceptionContext.HttpContext.Request.RequestType == "POST")
                        {
                            //handler POST fail
                            HandlePostFail(exceptionContext, urlRoute, "Something went wrong.");

                        }
                        else
                        {
                            HandleGetFail(exceptionContext, urlRoute, "");
                        }
                        break;
                }

            }

        }
    }
}