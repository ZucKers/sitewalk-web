﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuikSiteWalkModel1;
using BuikSiteWalkModel1.CustomExceptions;

namespace DemoBuikAlp1.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class UserAuthorizeAttribute : FilterAttribute, IAuthorizationFilter
    {
        //is log in?
        //is lost session?
        ///<summary>
        /// flag บอกว่าเวลาตรวจสอบสิทธิ์ ต้องรวมการตรวจสอบว่ามีการเลือกบริษัทมาแล้วหรือยัง
        ///</summary>
        public bool NotNeedCompany
        {
            get;
            set;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            //เช็คว่าล็อคอินแล้วหรือยัง
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            var sessionHandler = new UserSession(filterContext.HttpContext);

            //เช็คความผิดพลาดของ session
            if (sessionHandler.UserInfoKeySurrKey == 0)
                throw new SessionLostException();

        }
    }
}