﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;

namespace DemoBuikAlp1.StorageService
{
    public class BlobStorageService
    {
        private string strconn ="";
        public BlobStorageService() {
        this.strconn = ConfigurationManager.AppSettings["StorageConnectionString"];
        }
        public BlobStorageService(string strconn)
        {
            this.strconn = strconn;
        }
        public CloudBlobContainer GetCloudBlobContainer(string name)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(strconn
                    //ConfigurationManager.AppSettings["StorageConnectionString"]
                    );
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(name);
                if (blobContainer.CreateIfNotExists())
                {
                    blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }
                return blobContainer;
            }

        public CloudBlobContainer GetCloudBlobContainer2(string name)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                    ConfigurationManager.AppSettings["StorageConnectionString2"]
                    );
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(name);
                if (blobContainer.CreateIfNotExists())
                {
                    blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }
                return blobContainer;
            }

        }
    }
