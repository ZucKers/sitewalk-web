﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoBuikAlp1.Models
{
    public class ProjectLinkModel
    {
        public string ProjectName { get; set; }
        public string ProjectId { get; set; }
        public bool IsFav { get; set; }
    }
}