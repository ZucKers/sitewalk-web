﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuikSiteWalkModel1.ModelEntities;

namespace DemoBuikAlp1.Models
{
    public class ProjectIssueModel
    {
        public ProjectDetailMobile prdetail { get; set; }
        public List<BuikIssueEntity> IssueList { get; set; }

        //Project ID added by KN
        public int projectID { get; set; }
    }
}