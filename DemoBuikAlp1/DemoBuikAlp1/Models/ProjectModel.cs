﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoBuikAlp1.Models
{
    public class ProjectModel
    {
        [Required]
        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }

        [Required]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Project Type")]
        public string ProjectType { get; set; }

        [Display(Name = "Location")]
        public string Country { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }
        
        [Display(Name = "Province")]
        public string Province { get; set; }

        [Display(Name = "Latitude")]
        public decimal Lat { get; set; }

        [Display(Name = "Longtitude")]
        public decimal Long { get; set; }

        public byte[] ImageData { get; set; }
    }
}