﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuikSiteWalkModel1.ModelEntities;

namespace DemoBuikAlp1.Models
{
    public class FloorPlanPhotoModel
    {
        public List<IntStringObj> photolist { get; set; }
        public Int32 prid { get; set; }
        public string name { get; set; }

    }
}