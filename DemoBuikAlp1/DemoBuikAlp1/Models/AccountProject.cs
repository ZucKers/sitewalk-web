﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuikSiteWalkModel1.ModelEntities;

namespace DemoBuikAlp1.Models
{
    public class AccountProject
    {
        public BuikUserEntity userentity{get;set;}
        public List<ProjectDetailMobile> projectinhand { get; set; }

        public int coutnfav()
        {
            return projectinhand.Where(p => p.isFav == 1).Count();
        }
    }
}