function checkbox() {
    $('input.checkbox').each(function () {
        $(this).after('<i class="checkbox icon-check-empty" id="' + $(this).attr('id') + '"></i>'); if ($(this).attr('checked')) { $(this).next().removeClass('icon-check-empty').addClass('icon-check'); }
        if ($(this).attr('disabled')) { $(this).next().addClass('disabled'); }
        $(this).remove();
    }); $('i.checkbox').click(function () { $(this).toggleClass('icon-check').toggleClass('icon-check-empty'); }); $('label.checkbox').click(function () { $('#' + $(this).attr("for")).toggleClass('icon-check').toggleClass('icon-check-empty'); });
}
function radio() {
    $('input.radio').each(function () {
        $(this).after('<i class="radio icon-radio-empty" name="' + $(this).attr('name') + '" id="' + $(this).attr('id') + '"></i>'); if ($(this).attr('checked')) { $(this).next().removeClass('icon-radio-empty').addClass('icon-radio'); }
        if ($(this).attr('disabled')) { $(this).next().addClass('disabled'); }
        $(this).remove();
    }); $('i.radio').click(function () { $('i.radio[name=' + $(this).attr("name") + ']').removeClass('icon-radio').addClass('icon-radio-empty'); $(this).removeClass('icon-radio-empty').addClass('icon-radio'); }); $('label.radio').click(function () { $('i.radio[name=' + $("#" + $(this).attr("for")).attr("name") + ']').removeClass('icon-radio').addClass('icon-radio-empty'); $('#' + $(this).attr("for")).removeClass('icon-radio-empty').addClass('icon-radio'); });
}
function select() {
    $('select').wrapInner('<div class="select" />')
    $('div.select').unwrap().prepend('<p></p><i class="icon-chevron-down"></i>'); $('option').wrapInner('<li />'); $('option li').unwrap().wrapAll('<ul />'); $('div.select ul').hide(); $('div.select').click(function () { $('div.select').toggleClass('active'); $('div.select ul').toggle(); }); $('div.select li').click(function () { $('div.select p').empty().append($(this).text()); });
}
$(document).ready(function () { checkbox(); radio(); });