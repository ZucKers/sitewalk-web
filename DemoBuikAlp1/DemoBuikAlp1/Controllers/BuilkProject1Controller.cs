﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuikSiteWalkModel1.ModelEntities;
using BuikSiteWalkModel1.Repository;
using BuikSiteWalkModel1.Abstract;
using Microsoft.WindowsAzure.Storage.Blob;
using DemoBuikAlp1.StorageService;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;

namespace DemoBuikAlp1.Controllers
{   
    //[Authorize]
    public class BuilkProject1Controller : Controller
    {
        EFIssueRepository irepo = new EFIssueRepository();
        IUserRepository urepo = new EFUserRepository();
        IProjectRepository prepo = new EFProjectRepository();
        

        #region MobileService

        private List<BuikProjectEntity> GetProjectListByEmail(string uname)
        {
            List<BuikProjectEntity> res = new List<BuikProjectEntity>();
            try
            {
                res = prepo.GetProjectMobileEntityByCollaborationIdEmail(uname);
            }
            catch { return res; }
            return res;
        }
        private ActionResult ProjectViewMobile(string uemail)
        {
            List<BuikProjectEntity> r = prepo.GetProjectMobileEntityByCollaborationIdEmail(uemail);

            return Json(r, JsonRequestBehavior.AllowGet);
        }
        private int pAddProjectIssueComment(string comment, int issueid, int userid)
        {
            return irepo.AddProjIssueComment(userid, issueid, comment);
        }
        private int pAddIssueLike(Int32 issueid, Int32 userid)
        {
            return irepo.AddLikeIssue(issueid, userid);
        }

        public ActionResult GetAllProjectType()
        {
            var res = prepo.GetAllProjectType();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IndexMobileNew(string email)
        {
            var p = prepo.GetProjectDetailMobileByEmail(email);//GetProjectListByEmail(uname);
            
            return Json(p,JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllProjectList(string email)
        {
            var p = prepo.GetProjectMobileEntityByCollaborationIdShowAll(urepo.GetUserIdByEmail(email));//GetProjectListByEmail(uname);

            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjectCompany(Int32 prid, Int32 compid)
        {
            try
            {
                return Json(prepo.UpdateProjectCompany(prid, compid),JsonRequestBehavior.AllowGet);
            }
            catch { return Json("0",JsonRequestBehavior.AllowGet); }
        }

        public ActionResult GetProjectDetailById(Int32 prid)
        { 
            var p = prepo.GetProjectDetailByPrid(prid);
            return Json(p, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetProjectAsFav(Int32 uid,Int32 prid) 
        {
            return Json(prepo.SetProjAsFav(uid, prid, 1),JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnsetProjAsFav(Int32 uid, Int32 prid) 
        {
            return Json(prepo.SetProjAsFav(uid, prid, 0),JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetProjectShow(Int32 uid, Int32 prid)
        {
            return Json(prepo.SetCollaborationShow(prid, uid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SetProjectHide(Int32 uid, Int32 prid)
        {
            return Json(prepo.SetCollaborationHide(prid, uid),JsonRequestBehavior.AllowGet);
        }


        public ActionResult IndexMobile()
        {
            //var p = prepo.GetProjectMobileEntityByCollaborationId((string)Session["UserDisplayName"]);
            List<BuikProjectEntity> p = new List<BuikProjectEntity>(); 
            if(Session["UserEmail"] != null)
            {
                p = GetProjectListByEmail(Session["UserEmail"].ToString()).OrderByDescending(r=> r.IsFav).ThenBy(q => q.prid).ToList();
            }
            return Json(p,JsonRequestBehavior.AllowGet);
        }       

        public ActionResult AddNewProjectMobile(Int32 userid,string pname,string cname,string ptype,string pprovince,string pcountry,string psdate,string pedate)
        {
            if(Session["UserDisplayName"] == null)
                Session["UserDisplayName"] = "Anonymous";
            BuikProjectEntity p = new BuikProjectEntity();
            p.ProjectName = pname;
            p.CustomerName = cname;
            p.ProjectType = ptype;
            p.ProjectCountry = pcountry;
            p.ProjectProvince = pprovince;
            p.ProjectStartDate = psdate;
            p.ProjectEndDate = pedate;
            //p.CreateUserName = userid;//Session["UserDisplayName"].ToString();
            p.createuserid = userid;
            string res = prepo.AddNewProjectMobile(p);
            if (Convert.ToInt32(res) < 1)
            {
                res = "Unable to Add Project Data";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {  Session["ProjectID"] = res;
                 
            return Json(res,JsonRequestBehavior.AllowGet);}
        }

        public ActionResult AddNewProjectMobileWithLat(Int32 userid, string pname, string cname, string ptype, string pprovince, string pcountry, string psdate, string pedate,string lat,string longt)
        {
           
            BuikProjectEntity p = new BuikProjectEntity();
            p.ProjectName = pname;
            p.CustomerName = cname;
            p.ProjectType = ptype;
            p.ProjectCountry = pcountry;
            p.ProjectProvince = pprovince;
            p.ProjectStartDate = psdate;
            p.ProjectEndDate = pedate;
            //p.CreateUserName = userid;//Session["UserDisplayName"].ToString();
            p.createuserid = userid;
            p.latitude = Convert.ToDecimal(lat);
            p.longtitude = Convert.ToDecimal(longt);

            string res = prepo.AddNewProjectMobileWithLat(p);
            if (Convert.ToInt32(res) < 1)
            {
                res = "Unable to Add Project Data";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
               // Session["ProjectID"] = res;
                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }
       

        public ActionResult AddNewProjectMobileWithPhoto(Int32 userid, string pname, string cname, string ptype, string pprovince, string pcountry, string psdate, string pedate,HttpPostedFileBase pcoverphoto)
        {
            if (Session["UserDisplayName"] == null)
                Session["UserDisplayName"] = "Anonymous";
            BuikProjectEntity p = new BuikProjectEntity();
            p.ProjectName = pname;
            p.CustomerName = cname;
            p.ProjectType = ptype;
            p.ProjectCountry = pcountry;
            p.ProjectProvince = pprovince;
            p.ProjectStartDate = psdate;
            p.ProjectEndDate = pedate;
            //p.CreateUserName = Session["UserDisplayName"].ToString();
            p.createuserid = userid;
            p.ProjectTimelinePhoto = SavePhotoToStorage(pcoverphoto,"CoverPhoto");
            string res = prepo.AddNewProjectMobile(p);
            if (Convert.ToInt32(res) < 1)
            {
                res = "Unable to Add Project Data";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Session["ProjectID"] = res;

                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult AddNewProjectMobileWithPhotoPath(Int32 userid, string pname, string cname, string ptype, string pprovince, string pcountry, string psdate, string pedate, string photopath)
        {
            if (Session["UserDisplayName"] == null)
                Session["UserDisplayName"] = "Anonymous";
            BuikProjectEntity p = new BuikProjectEntity();
            p.ProjectName = pname;
            p.CustomerName = cname;
            p.ProjectType = ptype;
            p.ProjectCountry = pcountry;
            p.ProjectProvince = pprovince;
            p.ProjectStartDate = psdate;
            p.ProjectEndDate = pedate;
            //p.CreateUserName = Session["UserDisplayName"].ToString();
            p.createuserid = userid;
            p.ProjectTimelinePhoto = photopath;//SavePhotoToStorage(pcoverphoto, "CoverPhoto");
            string res = prepo.AddNewProjectMobile(p);
            if (Convert.ToInt32(res) < 1)
            {
                res = "Unable to Add Project Data";
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Session["ProjectID"] = res;

                return Json(res, JsonRequestBehavior.AllowGet);
            }
        } 
        
        //Not Implemented
        public ActionResult AddProjectCoverPhotoMobilePath(string pathphoto, int projectid)
        {
            //string fpath = SavePhotoToStorage(photo, "CoverPhoto");
            string res = prepo.UpdateProjectProfilePhoto(projectid, pathphoto);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public string AddProjecFloorPalnMobileEE(HttpPostedFileBase photo, int projectid)
        {
            string fpath = SavePhotoToStorage(photo, "FloorPlan"+projectid.ToString());
            string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            return res;// Json(res, JsonRequestBehavior.AllowGet); 
        }

        public ActionResult AddProjecFloorPalnMobilePath(string pathphoto, int projectid)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            string res = prepo.UpdateProjectProfilePhoto(projectid, pathphoto);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddProjectCollaborationMobile1(string projectid,string invitedusermail)
        {
            //Session["UserDisplayName"] = urepo.GetUserEntitybyEmail(model.UserName);
            
            BuikProjectCollaborationEntity en = new BuikProjectCollaborationEntity();
            en.InvitedUserid = prepo.GetUserIdByEmail(invitedusermail);
            en.InviterId = prepo.GetUserIdByEmail(Session["UserEmail"].ToString());
            en.InviteStatus = 1;
            en.ProjectId = Convert.ToInt32(projectid);//(int)Session["ProjectId"];
            try
            {
                prepo.AddProjectCollaboration(projectid, invitedusermail);
            }
            catch { Json("0", JsonRequestBehavior.AllowGet); }
            return Json("1",JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddProjectCollaborationMobile2(string projectid, string invitedusermail,string inviteremail)
        {
            //Session["UserDisplayName"] = urepo.GetUserEntitybyEmail(model.UserName);

            BuikProjectCollaborationEntity en = new BuikProjectCollaborationEntity();
            en.InvitedUserid = prepo.GetUserIdByEmail(invitedusermail);
            en.InviterId = prepo.GetUserIdByEmail(inviteremail);
            en.InviteStatus = 1;
            en.ProjectId = Convert.ToInt32(projectid);//(int)Session["ProjectId"];
            try
            {
                return Json(prepo.AddProjectCollaboration(en),JsonRequestBehavior.AllowGet);
            }
            catch { return Json("0", JsonRequestBehavior.AllowGet); }
            //return Json("1",JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchUserMobile(string name)
        {
            List<IntStringObj> res = urepo.SearchUserName(name);
            return Json(res,JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchUseOrEmailMobile(string nameoremail)
        {
            List<IntStringObj> res = urepo.SearchUserNameOrEmail(nameoremail);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchUserEmailMobile(string email)
        {
            List<IntStringObj> res = urepo.SearchUserName(email);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddProjectIssueComment(string comment, int issueid,int userid)
        {
           
            if (this.pAddProjectIssueComment(comment, issueid, userid) == 0)
            {
                return Json("Error Add Comment", JsonRequestBehavior.AllowGet);
            }
            return Json("Comment added",JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteIssueComment(Int32 commentid)
        {
            return Json(prepo.RemoveIssueComment(commentid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddProjectIssueLike(Int32 issueid, Int32 userid) {
            return Json(this.pAddIssueLike(issueid, userid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemovePorjectIssueLike(Int32 issueid, Int32 userid)
        {
            return Json(prepo.RemoveLike(issueid, userid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIssueTypeMobile()
        {
            var res = irepo.GetIssueType();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        /*
        public ActionResult AddRequestForInformationIssue(int projectid,int issuetype,string issuedetail)
        {
            int uid; 
            try {
                uid = prepo.GetUserIdByEmail(Session["UserEmail"].ToString());
                
            }
            catch { return Json("Error Add Comment", JsonRequestBehavior.AllowGet); }
            if (irepo.AddIssueRequestForInformation(projectid,issuetype,issuedetail,uid) == 0)
            {
                return Json("Error Add Comment", JsonRequestBehavior.AllowGet);
            }
            return Json("Comment added", JsonRequestBehavior.AllowGet);
           
        }
        public ActionResult AddGoodJobIssue(int projectid, int issuetype, string issuedetail)
        {
            int uid;
            try
            {
                uid = prepo.GetUserIdByEmail(Session["UserDisplayName"].ToString());

            }
            catch { return Json("Error Add Comment", JsonRequestBehavior.AllowGet); }
            if (irepo.AddIssueGoodJob(projectid, issuetype, issuedetail, uid) == 0)
            {
                return Json("Error Add Comment", JsonRequestBehavior.AllowGet);
            }
            return Json("Comment added", JsonRequestBehavior.AllowGet);

        }
        public ActionResult AddProgressIssue(int projectid, int issuetype, int progresspercent)
        {
            int uid;
            try
            {
                uid = prepo.GetUserIdByEmail(Session["UserDisplayName"].ToString());

            }
            catch { return Json("Error Add Comment", JsonRequestBehavior.AllowGet); }
            if (irepo.AddIssueProgress(projectid, issuetype, progresspercent, uid) == 0)
            {
                return Json("Error Add Comment", JsonRequestBehavior.AllowGet);
            }
            return Json("Comment added", JsonRequestBehavior.AllowGet);
        }*/

        private string SavePhotoToStorage(HttpPostedFileBase image,string containername)
        {
            string buri;
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("DemoSiteWalk1");
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);
                buri = blob.Uri.ToString();
                return buri;
            }
            else return "No Image";
        }

        public ActionResult AddProjectIssueWtihPhotoAdnFloorPlan(int projectid, int issuetype, string issuedetail, Int32 userid,int prog, HttpPostedFileBase issuefile, HttpPostedFileBase issuefloorplan,string dl)
        {
            string f1 = this.SavePhotoToStorage(issuefile, "Issue" + projectid.ToString());
            string f2 = this.SavePhotoToStorage(issuefile, "IssueFloorPlan" + projectid.ToString());
            var res = irepo.AddProjectIssue(projectid, issuetype, issuedetail, userid, f1, f2,dl,prog,Path.GetExtension(issuefile.FileName));
            return Json(res.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddProjectIssueWithPhoto(int projectid, int issuetype, string issuedetail, Int32 userid,int prog,HttpPostedFileBase issuefile, string issuefloorplan,string deadline)
        {
            string issuefilelocation = this.SavePhotoToStorage(issuefile,"Issue"+projectid.ToString());
            var res = irepo.AddProjectIssue(projectid, issuetype, issuedetail, userid, issuefilelocation, issuefloorplan, deadline, prog, Path.GetExtension(issuefile.FileName));
            return Json(res.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddProjectIssue(Int32 projectid, int issuetype, string issuedetail,Int32 userid,int prog,string issuefile,string issuefloorplan,string deadline,string filetype)
        {
            var res = irepo.AddProjectIssue(projectid, issuetype, issuedetail,userid, issuefile, issuefloorplan,deadline,prog,filetype);
            return Json(res.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddIssuePersonIncharged(Int32 issueid,Int32 userid,string deadlinedate)
        {
            var res = irepo.AddPersonInChargedIssue(issueid, userid, 1, deadlinedate);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProjectFloorPlanList(Int32 prid)
        {
            var res = prepo.GetProjectFloorPlan(prid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        
        public ActionResult GetProjectIssueList(Int32 prid)
        {
           // if (prid == 2013009) { return Json(prepo.GetProjectIssueListTestScript(), JsonRequestBehavior.AllowGet); }
           // int pagingsize = 20; 


            var res = prepo.GetProjectIssueList(prid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetPrjoectIssueList(Int32 prid)
        {
            // if (prid == 2013009) { return Json(prepo.GetProjectIssueListTestScript(), JsonRequestBehavior.AllowGet); }
            // int pagingsize = 20; 


            var res = prepo.GetProjectIssueList(prid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProjectIssueListLoadMore(Int32 prid,int page = 1)
        {
            // if (prid == 2013009) { return Json(prepo.GetProjectIssueListTestScript(), JsonRequestBehavior.AllowGet); }
            int pagingsize = 20;

            var res = prepo.GetProjectIssueList(prid,pagingsize,page);
            
            //var res = prepo.GetProjectIssueList(prid);
            return Json(res.OrderByDescending(p => p.IssueId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPrjoectIssueListLoadMore(Int32 prid, int page = 1)
        {
            // if (prid == 2013009) { return Json(prepo.GetProjectIssueListTestScript(), JsonRequestBehavior.AllowGet); }
            int pagingsize = 20;

            var res = prepo.GetProjectIssueList(prid, pagingsize, page);

            //var res = prepo.GetProjectIssueList(prid);
            return Json(res.OrderByDescending(p => p.IssueId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIssueDetail(string issueId)
        {
            var res = prepo.GetProjectIssueListByIssueId(issueId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIssueCommentDetail2(Int32 issueid)
        {
            var res = prepo.GetIssueCommentDetail2(issueid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIssueCommentDetail(Int32 issueid)
        {
            var res = prepo.GetIssueCommentDetail2(issueid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AcceptProjectCollaboration(Int32 userid, Int32 projectid,Int32 assignuid)
        {
            int res = 0;
            try { res = prepo.UpdateProjectCollaboration(projectid.ToString(), userid.ToString(), assignuid.ToString(), 2); }
            catch {return(Json("Accept Error",JsonRequestBehavior.AllowGet));}
            return Json("Accept Complete",JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetProjectCollaborationRequest(int userid)
        {
            List<BuikProjectCollaborationEntity> res = prepo.GetProjectCollaborationRequest(userid);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult SetProjectAsFav(Int32 userid, Int32 projectid)
        //{
        //    return Json(prepo.SetProjAsFav(userid, projectid,1), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetCollaboratoinListFromProject(Int32 prid, Int32 userid)
        {           
            try
            {
                List<ProjColList> collist = new List<ProjColList>();
                collist.AddRange(prepo.GetProjectCollabarationList(prid).Distinct());
                collist.AddRange(prepo.GetProjectEmailInvitedCollaboration(prid,userid));
                return Json(collist,JsonRequestBehavior.AllowGet );
            }
            catch{return Json("Error",JsonRequestBehavior.AllowGet);}

        }

        public ActionResult SetProjectAsUnFav(Int32 userid, Int32 projectid)
        {
            return Json(prepo.SetProjAsFav(userid, projectid, 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveProjectCollaboration(Int32 prid, Int32 userid,Int32 targetuserid)
        {
            if (userid == prepo.GetProjectCreator(prid))
            {
                if (userid == targetuserid)
                {
                    return Json("Cannot Remove Creator", JsonRequestBehavior.AllowGet);
                }
                else if (userid != targetuserid)
                {
                    return Json(prepo.RemoveProjectCollaboration(prid, targetuserid), JsonRequestBehavior.AllowGet);
                }
            }
           //else if (userid != prepo.GetProjColInviter(prid,targetuserid))
           //{
           return Json(prepo.RemoveProjectCollaboration(prid,targetuserid), JsonRequestBehavior.AllowGet);
           //}
           //return Json(0, JsonRequestBehavior.AllowGet);

        }

        public ActionResult RemoveEmailInvitation(string email, Int32 prid, Int32 inviterid)
        {
            if (inviterid == prepo.GetProjectCreator(prid))
            {
                return Json(prepo.RemoveEmailInvitation(email, prid, inviterid), JsonRequestBehavior.AllowGet);
            }
            return Json("Unable to delete", JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUserInfo(Int32 userid)
        {
            return Json(urepo.GetUserEntitybyId(userid), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProjectNameListbyUSerid(Int32 uid)
        {
            return Json(prepo.GetProjectListForUser(uid),JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemoveProjectIssue(string issueid, Int32 userid)
        {
            int res = 0;
            try
            {
                if (userid == irepo.GetProjectIssueCreator(issueid))
                {
                    res = irepo.RemoveProjectIssue(issueid);
                }
            }
            catch { return Json(0, JsonRequestBehavior.AllowGet); }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddNotification(string fuid,string tuid,int isR,string msg,string nf,string did)
        {
            int res = 0;
            try
            {
                res = prepo.AddNotification(fuid, tuid, isR, msg, nf, did);
            }
            catch { res =  0; }
            return Json(res,JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateNotification(Int32 id, int val)
        {
            int res = 0;
            try
            {
                res = prepo.UpdateNotifIsRead(id, val);
            }
            catch { res = 0; }
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNotificationByUserId(string userid)
        {
            return Json(prepo.GetNotificationById(userid), JsonRequestBehavior.AllowGet);
        }

       // public ActionResultTestPhotoUploadMobile()
        public ActionResult TestPhotoUploadMobile(HttpPostedFileBase image, string gg,int i)
        {
            string buri="";
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("TestPhoto1");
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);
                buri = blob.Uri.ToString();
            }
            return Json(buri,JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        BlobStorageService _blobStorageService = new BlobStorageService();
        [HttpPost]
        public string Upload(HttpPostedFileBase image, string blobcontainername)
        {
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(blobcontainername);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);
                string res = prepo.UpdateProjectProfilePhoto(Convert.ToInt32(blobcontainername), blob.Uri.ToString());
                return blob.Uri.ToString();
            }
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            return "Error uploading";//RedirectToAction("Upload");
        }
        [HttpPost]
        public string AddProjectCoverPhotoMobile(HttpPostedFileBase photo, string projectid)
        {
            string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (photo.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("CoverPhoto");
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(photo.FileName);
                blob.UploadFromStream(photo.InputStream);
                fpath = blob.Uri.ToString();
                string res = prepo.UpdateProjectProfilePhoto(Convert.ToInt32(projectid), fpath);
                return res;
            }
           
            return "Error Uploading";// Json(res, JsonRequestBehavior.AllowGet); 
        }

        [HttpPost]
        public string AddProjecFloorPalnMobile(HttpPostedFileBase photo, int projectid,string fpname)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (photo.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("CoverPhoto" + projectid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(photo.FileName);
                blob.UploadFromStream(photo.InputStream);
                fpath = blob.Uri.ToString();
                string res = prepo.AddProjectFloorPlanPhoto(Convert.ToInt32(projectid), fpath,fpname);
                return res;
            }

            return "Error Uploading";

        }

        [HttpPost]
        public string UploadFP(HttpPostedFileBase image,string projectid,string fpname)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(projectid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);
                
                string res = prepo.AddProjectFloorPlanPhoto(Convert.ToInt32(projectid), blob.Uri.ToString(),fpname);
                return blob.Uri.ToString();
            }

            return "Error Uploading";

        }

        [HttpPost]
        public string UploadIP(HttpPostedFileBase image, string issueid)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(issueid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);

                string res = irepo.UpdateProjectIssuePhoto(issueid, blob.Uri.ToString()).ToString();
                return blob.Uri.ToString();
            }

            return "Error Uploading";

        }

        [HttpPost]
        public string UploadIF(HttpPostedFileBase image, string issueid)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(issueid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);

                string res = irepo.UpdateProjectIssueFloorPlan(issueid, blob.Uri.ToString()).ToString();
                return blob.Uri.ToString();
            }

            return "Error Uploading";

        }
        public ActionResult UploadPP44()
        {
            return View();
        }
         [HttpPost]
        public string UploadPP44(HttpPostedFileBase image)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = prepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image.ContentLength > 0)
            {
                try
                {
                    BlobStorageService bb1 = new BlobStorageService(ConfigurationManager.AppSettings["StorageConnectionString2"]);
                    CloudBlobContainer blobContainer = bb1.GetCloudBlobContainer("upload");
                    CloudBlockBlob blob = blobContainer.GetBlockBlobReference(@"avatar/" + image.FileName);
                    blob.UploadFromStream(image.InputStream);
                    blob.Properties.ContentType = image.ContentType;
                    //int res = urepo.UpdateuserPhoto(image.FileName, 2009608);
                    return blob.Uri.ToString();// View(blob.Uri.ToString());
                }
                catch { return "erroe"; }//View(); }// "Error Update";
            }return "Ok";//View();
            }

        [HttpPost]
        public string UploadPP(HttpPostedFileBase image, string userid)
        {
            if (image.ContentLength > 0)
            {
                try
                {
                    BlobStorageService bb1 = new BlobStorageService(ConfigurationManager.AppSettings["StorageConnectionString2"]);
                    CloudBlobContainer blobContainer = bb1.GetCloudBlobContainer("upload");
                    CloudBlockBlob blob = blobContainer.GetBlockBlobReference(@"avatar/" + image.FileName);
                    blob.Properties.ContentType = image.ContentType;
                    blob.UploadFromStream(image.InputStream);

                    int res = urepo.UpdateuserPhoto(image.FileName, Convert.ToInt32(userid));
                    return blob.Uri.ToString();
                }
                catch { return "Error Update"; }
            }

            return "Error Uploading";

        }
        [HttpPost]
        public string UploadTvT(HttpPostedFileBase image)
        {
            
            CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer("upload");
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(@"avatar/" + image.FileName);
            blob.Properties.ContentType = image.ContentType;
            string name = image.FileName;

           // int maxSize = 1 * 1024 * 1024; // 4 MB
            int maxSize = 100 * 1024; // 4 MB
            if (image.ContentLength > maxSize)
            {

                byte[] data;
                using (Stream inputStream = image.InputStream)
                {
                    MemoryStream memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    data = memoryStream.ToArray();
                }
                
               // byte[] data = image.FileBytes; 
                int id = 0;
                int byteslength = data.Length;
                int bytesread = 0;
                int index = 0;
                List<string> blocklist = new List<string>();
                //int numBytesPerChunk = 250 * 1024; //250KB per block
                int numBytesPerChunk = 25 * 1024; //250KB per block
                do
                {
                    byte[] buffer = new byte[numBytesPerChunk];
                    int limit = index + numBytesPerChunk;
                    for (int loops = 0; index < limit; index++)
                    {
                        buffer[loops] = data[index];
                        loops++;
                    }
                    bytesread = index;
                    string blockIdBase64 = Convert.ToBase64String(System.BitConverter.GetBytes(id));

                    blob.PutBlock(blockIdBase64, new MemoryStream(buffer, true), null); 
                    blocklist.Add(blockIdBase64);
                    id++;
                } while (byteslength - bytesread > numBytesPerChunk);

                int final = byteslength - bytesread;
                byte[] finalbuffer = new byte[final];
                for (int loops = 0; index < byteslength; index++)
                {
                    finalbuffer[loops] = data[index];
                    loops++;
                }
                string blockId = Convert.ToBase64String(System.BitConverter.GetBytes(id));
                blob.PutBlock(blockId, new MemoryStream(finalbuffer, true), null);
                blocklist.Add(blockId);

                blob.PutBlockList(blocklist);
                int res = urepo.UpdateuserPhoto(image.FileName, 2009608);
                return blob.Uri.ToString();
            }
            else
                blob.UploadFromStream(image.InputStream);
            int i = urepo.UpdateuserPhoto(image.FileName, 2009608);
            return blob.Uri.ToString();
        }
        public  ActionResult UploadTvT() { return View(); }

        public ActionResult RemoveProjectFloorPlanPhoto(string fpath)
        { 
            return Json(prepo.RemoveFPPhoto(fpath),JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateUserInfo(Int32 userid,string name, string sname,string company,string btype)
        {

            try { return Json(urepo.UpdateUserDetail(userid, name, sname, company,btype),JsonRequestBehavior.AllowGet); }
            catch { return Json("Error Update", JsonRequestBehavior.AllowGet); }
        }


        public ActionResult GetPersonInIssue(Int32 issueid)
        {
            try
            {
                return Json(irepo.GetUserIssueInvolve(issueid), JsonRequestBehavior.AllowGet);
            }
            catch { return Json("Error", JsonRequestBehavior.AllowGet); }
        }

        public ActionResult GetUserCompanyListById(Int32 userid)
        {
            return Json(prepo.GetCompanyNamebyId(userid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCompanyNameByPrId(Int32 prid)
        {
            return Json(prepo.GetCompanyByProjectId(prid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjName(Int32 prid, string name)
        {
            return Json(prepo.EditProjectName(prid, name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjCustomer(Int32 prid, string name)
        {
            return Json(prepo.EditProjCustomer(prid, name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjCountry(Int32 prid, string name)
        {
            return Json(prepo.EditProjectCountry(prid, name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjProv(Int32 prid, string name)
        {
            return Json(prepo.EditProjectProvince(prid, name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjStartDate(Int32 prid, string dates)
        {
            return Json(prepo.EditProjectStartDate(prid, dates), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateProjEndDate(Int32 prid, string dates)
        {
            return Json(prepo.EditProjectEndDate(prid, dates), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCompanyName(Int32 prid)
        {
            return Json(prepo.GetCompanyByProjectId(prid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetVersion()
        {
            return Json(prepo.GetVersion(),JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetVersion(string v)
        { 
            return Json(prepo.SetVersion(v),JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLastPhotoIssue(Int32 prid)
        {
            return Json(prepo.GetLatestIssuePhoto(prid), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEmailInvitation(string email, Int32 uid, Int32 prid)
        {
            var test1 = urepo.SearchUserByEmail(email).Count();
            if (test1 == 0)
            {
                return Json(urepo.AddUserInvitation(email, uid, prid, "token"), JsonRequestBehavior.AllowGet);
            }

            else return Json("0", JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailInvitationResponse(string email, Int32 prid, Int32 userid)
        {
            Int32 ii = urepo.SearchUserEmail(email);
            if (ii != 0)
            {
                //if (!prepo.GetProjectCollaborationRequest(prid)
                {
                    int i = prepo.AddProjectCollaboration(prid, ii, 2, 0, userid);
                    if (i == 0)//prepo.AddProjectCollaboration(prid, ii, 2, 0, userid) == 0)
                    {
                        prepo.UpdateProjectCollaboration2(prid.ToString(), ii.ToString(), userid.ToString(), 2);
                    }
                }
            }
            else
            { urepo.AddUserInvitation(email, userid, prid, "token"); }

            //return Redirect("sitewalkapp://app.sitewalk.com");
            return new RedirectResult("sitewalkapp://app.sitewalk.com",false);
        }


        //public ActionResult ProjectViewMobile()
        //{
        //    List<BuikProjectEntity> r = prepo.GetProjectMobileEntityByCollaborationId(Session["UserDisplayName"].ToString());
        //    if (r.Count > 0)
        //    {
        //        return Json(r, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //        r = new List<BuikProjectEntity>();
        //    return Json(r, JsonRequestBehavior.AllowGet);
        //}
        #endregion


      
      
      
    }
}
