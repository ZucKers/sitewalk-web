﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoBuikAlp1.StorageService;
using Microsoft.WindowsAzure.Storage.Blob;
using DemoBuikAlp1.Models;
using BuikSiteWalkModel1;
using BuikSiteWalkModel1.Repository;
using BuikSiteWalkModel1.Abstract;
using BuikSiteWalkModel1.ModelEntities;
using DemoBuikAlp1.Helpers;

namespace DemoBuikAlp1.Controllers
{
    
    public class ProjectController : Controller
    {
        //
        // GET: /Project/

        IProjectRepository prepo = new EFProjectRepository();
        EFUserRepository urepo = new EFUserRepository();
        EFIssueRepository irepo = new EFIssueRepository();
       // [Authorize]
        // show main page after login, show all project that user are involved
        [HttpGet]
        public ActionResult Index()
        {
            //string em = Session["UserEmail"].ToString();
            if (Session["UserEmail"] == null)
            {
                Session["UserEmail"] = User.Identity.Name;
            }
            var tt = prepo.GetProjectDetailMobileByEmail(User.Identity.Name);
            return View(tt);
        }
        
        //show the project detail and project issue list.
        [HttpGet]
        public ActionResult ProjectDetail(Int32 prid)
        {
            ProjectIssueModel vv = new ProjectIssueModel();
            vv.prdetail = prepo.GetProjectDetailByPrid(prid);
            vv.IssueList = prepo.GetProjectIssueList(prid);
            return View(vv);
        }


        [HttpGet]
        public ActionResult IssueDetail(Int32 issueid)
        {
            List<BuikIssueComment> tt = prepo.GetIssueCommentDetail(issueid);
            return View(tt);
        }

        [HttpGet]
        public ActionResult IssueCommentDetail()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.ProjectTypes = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Resideltial House/Low-rise Building", Value = "1"},
                new SelectListItem(){ Text = "Condominium/High-rise Building", Value = "2"},
                new SelectListItem(){ Text = "Office/Commercial Building", Value = "3"},
                new SelectListItem(){ Text = "Road/Bridge, Infrastructure", Value = "4"},
                new SelectListItem(){ Text = "Other", Value = "5"}
            };
            return View();
        }
        //Int32 userid,string pname,string cname,string ptype,string pprovince,string pcountry,string psdate,string pedate
        [HttpPost]
        public ActionResult Create(ProjectModel p, HttpPostedFileBase image44) 
        {
            Int32 uid = urepo.GetUserIdByEmail(User.Identity.Name);
            string r = p.ProjectName;
            string ff = p.StartDate.ToString();
            //AddNewProjectMobileWithLat(Int32 userid, string pname, string cname, string ptype, string pprovince, string pcountry, string psdate, string pedate,string lat,string longt)
            BuikProjectEntity pp = new BuikProjectEntity();
            pp.ProjectName = p.ProjectName;// pname;
            pp.CustomerName = p.CustomerName;// cname;
            pp.ProjectType = p.ProjectType.ToString();// ptype;
            pp.ProjectCountry = "THA";//p.Country.ToString();// pcountry;
            pp.ProjectProvince = p.Province;
            pp.ProjectStartDate = p.StartDate.ToShortDateString();
            pp.ProjectEndDate = p.EndDate.ToShortTimeString();
            //p.CreateUserName = userid;//Session["UserDisplayName"].ToString();
            pp.createuserid = uid;
            pp.latitude = Convert.ToDecimal(p.Lat);
            pp.longtitude = Convert.ToDecimal(p.Long);
            pp.ProjectTimelinePhoto = UploadPhotoCover(image44);
            string res = prepo.AddNewProjectMobileWithLat(pp);
            if (Convert.ToInt32(res) < 1)
            {
                //res = "Unable to Add Project Data";
                return RedirectToAction("Create","Proejct");//Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Session["ProjectID"] = res;
                return RedirectToAction("Index", "Project");//, new { prid = en.ProjectSurrKey }));
            }
            //string gg = 
            
        }

        [HttpPost]
        public ActionResult Create2(ProjectModel p)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create3(ProjectModel p)
        {
            return View();
        }

        private string UploadPhotoCover(HttpPostedFileBase image)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("coverphoto");
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
            blob.UploadFromStream(image.InputStream);
            string uri = blob.Uri.ToString();
            return uri;
        }

        [HttpPost]
        public ActionResult UploadCoverPhoto(HttpPostedFileBase image)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("coverphoto");
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
            blob.UploadFromStream(image.InputStream);
            string uri = blob.Uri.ToString();
            return PartialView("_PhotoUploaded", new List<UploadPhotoModel>() { new UploadPhotoModel() { PhotoUrl = uri } });
        }

        [HttpPost]
        public ActionResult UploadFloorPlan(HttpPostedFileBase[] images)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("floorplanphoto");
            var uploadPhotos = new List<UploadPhotoModel>();
            foreach (var img in images)
            {
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(img.FileName);
                blob.UploadFromStream(img.InputStream);
                string uri = blob.Uri.ToString();
                uploadPhotos.Add(new UploadPhotoModel() { PhotoUrl = uri });
            }
            return PartialView("_PhotoUploaded", uploadPhotos);
        }
        
        [HttpGet]
        public ActionResult CreateProjectIssue()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult CreateProjectIssue(BuikIssueEntity en, HttpPostedFileBase image44)
        {
            Int32 uid = urepo.GetUserIdByEmail(User.Identity.Name);
            Int32 i = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId,en.IssueDetail,uid ,en.IssueFile, en.IssueFloorPlanPhoto, en.IssueDeadline, en.IssueProgress,en.IssueFileType);

            //var int i = irepo.AddProjectIssue(projectid, issuetype, issuedetail, userid, issuefile, issuefloorplan, deadline, prog, filetype);
            
            return RedirectToAction("ProjectDetail", "Project", new { prid = en.ProjectSurrKey});
        }

        [HttpPost]
        public ActionResult AddIssueComment(Int32 issueId, string issuecomment)
        {
            return View();
        }

        [HttpPost]
        public ActionResult IssueClickedLike(Int32 issueId)
        {
            return View();
        }
    }
}
