﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuikSiteWalkModel1.Repository;


using System.IO;

using System.Security.Permissions;

//using System.Windows;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.Test.Tools.WicCop.InteropServices.ComTypes;
using DemoBuikAlp1.InteropServices;
using System.Net;
using BuikSiteWalkModel1.Abstract;

namespace DemoBuikAlp1.Controllers
{
    public class PhotoRezController : Controller
    {
        //
        // GET: /PhotoRez/
        private const uint ThumbnailSize = 500;
        public ActionResult Index()
        {
            return View();
        }

        public FileStreamResult GetIssueImage(Int32 issueid)
        {
            EFIssueRepository irepo = new EFIssueRepository();
            string photoPath = irepo.GetIssuePhotoFile(issueid);
        try {
            // Read the source image
            //var photo = System.IO.File.ReadAllBytes(photoPath);
            var webClient = new WebClient();
            byte[] photo = webClient.DownloadData(photoPath);//"http://www.google.com/images/logos/ps_logo2.png");
            var factory = (IWICComponentFactory)new WICImagingFactory();
            var inputStream = factory.CreateStream();
            inputStream.InitializeFromMemory(photo, (uint)photo.Length);
            var decoder = factory.CreateDecoderFromStream(inputStream, null, WICDecodeOptions.WICDecodeMetadataCacheOnLoad);
            var frame = decoder.GetFrame(0);
            // Compute target size
            uint width, height, thumbWidth, thumbHeight;
            frame.GetSize(out width, out height);
            if (width > height) {
                thumbWidth = ThumbnailSize;
                thumbHeight = height * ThumbnailSize / width;
            }
            else {
                thumbWidth = width * ThumbnailSize / height;
                thumbHeight = ThumbnailSize;
            }
            // Prepare output stream to cache file
            var outputStream = new MemoryIStream();
            // Prepare JPG encoder
            var encoder = factory.CreateEncoder(Consts.GUID_ContainerFormatJpeg, null);
            encoder.Initialize(outputStream, WICBitmapEncoderCacheOption.WICBitmapEncoderNoCache);
            // Prepare output frame
            IWICBitmapFrameEncode outputFrame;
            var arg = new IPropertyBag2[1];
            encoder.CreateNewFrame(out outputFrame, arg);
            var propBag = arg[0];
            var propertyBagOption = new PROPBAG2[1];
            propertyBagOption[0].pstrName = "ImageQuality";
            propBag.Write(1, propertyBagOption, new object[] { 0.85F });
            outputFrame.Initialize(propBag);
            outputFrame.SetResolution(96, 96);
            outputFrame.SetSize(thumbWidth, thumbHeight);
            // Prepare scaler
            var scaler = factory.CreateBitmapScaler();
            scaler.Initialize(frame, thumbWidth, thumbHeight, WICBitmapInterpolationMode.WICBitmapInterpolationModeFant);
            // Write the scaled source to the output frame
            outputFrame.WriteSource(scaler, new WICRect { X = 0, Y = 0, Width = (int)thumbWidth, Height = (int)thumbHeight });
            outputFrame.Commit();
            encoder.Commit();
            var outputArray = outputStream.ToArray();
            outputStream.Close();
            // Ouput cache
            // (cache) {
            //    OutputCacheResponse(
            //        context, System.IO.File.GetLastWriteTime(photoPath));
            //}
            // Write to the cache file
            //System.IO.File.WriteAllBytes(photoCachePath, outputArray);
            // Write to the response
           // context.Response.ContentType = "image/jpg";
           // context.Response.OutputStream.Write(outputArray, 0, outputArray.Length);
           // FileStreamResult res = outputStream.Write(outputArray, 0, outputArray.Length);
            Response.ContentType = "image/jpg";
            Response.OutputStream.Write(outputArray, 0, outputArray.Length);
            //FileStreamResult res = new FileStreamResult(outputStream,"image/jpg");
            //return res;// outputStream.W;
        }
        catch (FileNotFoundException) {
            Throw404();
        }

        return null;
        //    //var photo = File.ReadAllBytes(photoPath);
        //    //var factory =
        //    //    (IWICComponentFactory)new WICImagingFactory();
        //    //var inputStream = factory.CreateStream();
        //    //inputStream.InitializeFromMemory(photo,
        //    //    (uint)photo.Length);
        //    //var decoder = factory.CreateDecoderFromStream(
        //    //    inputStream, null,
        //    //    WICDecodeOptions.WICDecodeMetadataCacheOnLoad);
        //    //var frame = decoder.GetFrame(0);
        }

        //public byte[] ToByteArray(BitmapFrame targetFrame)
        //{
        //    byte[] targetBytes = null;
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        var targetEncoder = new JpegBitmapEncoder
        //        {
        //            QualityLevel = quality
        //        };
        //        var targetEncoder = new PngBitmapEncoder();
        //        targetEncoder.Frames.Add(targetFrame);
        //        targetEncoder.Save(memoryStream);
        //        targetBytes = memoryStream.ToArray();
        //    }
        //    return targetBytes;
        //}
        private static void Throw404()
        {
            throw new HttpException(404, "Photo not found.");
        }

        private static void OutputCacheResponse(HttpContext context, DateTime lastModified)
        {
            var cachePolicy = context.Response.Cache;
            cachePolicy.SetCacheability(HttpCacheability.Public);
            cachePolicy.VaryByParams["p"] = true;
            cachePolicy.VaryByParams["nocache"] = true;
            cachePolicy.SetOmitVaryStar(true);
            cachePolicy.SetExpires(DateTime.Now + TimeSpan.FromDays(365));
            cachePolicy.SetValidUntilExpires(true);
            cachePolicy.SetLastModified(lastModified);
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        // Knothing Added this 13/3/2014
        public FileStreamResult GetProjectCoverRez(Int32 prid)
        {
            IProjectRepository prepo = new EFProjectRepository();
            string photoPath = prepo.GetProjectCoverPhoto(prid);
            try
            {
                // Read the source image
                //var photo = System.IO.File.ReadAllBytes(photoPath);
                var webClient = new WebClient();
                byte[] photo = webClient.DownloadData(photoPath);//"http://www.google.com/images/logos/ps_logo2.png");
                var factory = (IWICComponentFactory)new WICImagingFactory();
                var inputStream = factory.CreateStream();
                inputStream.InitializeFromMemory(photo, (uint)photo.Length);
                var decoder = factory.CreateDecoderFromStream(inputStream, null, WICDecodeOptions.WICDecodeMetadataCacheOnLoad);
                var frame = decoder.GetFrame(0);
                // Compute target size
                uint width, height, thumbWidth, thumbHeight;
                frame.GetSize(out width, out height);
                if (width > height)
                {
                    thumbWidth = ThumbnailSize;
                    thumbHeight = height * ThumbnailSize / width;
                }
                else
                {
                    thumbWidth = width * ThumbnailSize / height;
                    thumbHeight = ThumbnailSize;
                }
                // Prepare output stream to cache file
                var outputStream = new MemoryIStream();
                // Prepare JPG encoder
                var encoder = factory.CreateEncoder(Consts.GUID_ContainerFormatJpeg, null);
                encoder.Initialize(outputStream, WICBitmapEncoderCacheOption.WICBitmapEncoderNoCache);
                // Prepare output frame
                IWICBitmapFrameEncode outputFrame;
                var arg = new IPropertyBag2[1];
                encoder.CreateNewFrame(out outputFrame, arg);
                var propBag = arg[0];
                var propertyBagOption = new PROPBAG2[1];
                propertyBagOption[0].pstrName = "ImageQuality";
                propBag.Write(1, propertyBagOption, new object[] { 0.85F });
                outputFrame.Initialize(propBag);
                outputFrame.SetResolution(96, 96);
                outputFrame.SetSize(thumbWidth, thumbHeight);
                // Prepare scaler
                var scaler = factory.CreateBitmapScaler();
                scaler.Initialize(frame, thumbWidth, thumbHeight, WICBitmapInterpolationMode.WICBitmapInterpolationModeFant);
                // Write the scaled source to the output frame
                outputFrame.WriteSource(scaler, new WICRect { X = 0, Y = 0, Width = (int)thumbWidth, Height = (int)thumbHeight });
                outputFrame.Commit();
                encoder.Commit();
                var outputArray = outputStream.ToArray();
                outputStream.Close();
                // Ouput cache
                // (cache) {
                //    OutputCacheResponse(
                //        context, System.IO.File.GetLastWriteTime(photoPath));
                //}
                // Write to the cache file
                //System.IO.File.WriteAllBytes(photoCachePath, outputArray);
                // Write to the response
                // context.Response.ContentType = "image/jpg";
                // context.Response.OutputStream.Write(outputArray, 0, outputArray.Length);
                // FileStreamResult res = outputStream.Write(outputArray, 0, outputArray.Length);
                Response.ContentType = "image/jpg";
                Response.OutputStream.Write(outputArray, 0, outputArray.Length);
                //FileStreamResult res = new FileStreamResult(outputStream,"image/jpg");
                //return res;// outputStream.W;
            }
            catch (FileNotFoundException)
            {
                Throw404();
            }

            return null;
        }
    }
}
