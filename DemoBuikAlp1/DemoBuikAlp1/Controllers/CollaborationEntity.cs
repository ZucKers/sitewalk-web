﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BuikSiteWalkModel1.ModelEntities;

namespace DemoBuikAlp1.Models
{
    public class CollaborationEntity
    {
        public List<ProjColList> prcollist { get; set; }
        public List<IntStringObj> searchlist { get; set; }
    }
}