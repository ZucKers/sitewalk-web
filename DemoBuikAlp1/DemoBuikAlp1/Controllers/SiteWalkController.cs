﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuikSiteWalkModel1.Abstract;
using BuikSiteWalkModel1.Repository;
using DemoBuikAlp1.Models;
using DemoBuikAlp1.StorageService;
using BuikSiteWalkModel1.ModelEntities;
using Microsoft.WindowsAzure.Storage.Blob;
using DemoBuikAlp1.Attributes;
using BuikSiteWalkModel1;

namespace DemoBuikAlp1.Controllers
{
    [ErrorHandler, UserAuthorize]
    public class SiteWalkController : Controller
    {
        //
        // GET: /SiteWalk/
     
        private IUserRepository UserRepo { get; set; }
        private IProjectRepository ProjectRepo { get; set; }
        EFIssueRepository irepo = new EFIssueRepository();
        EFProjectRepository prepo = new EFProjectRepository();

        private UserSession UserSession { get; set; }
       

        public SiteWalkController()
        {
            Initialize();
        }

        private void Initialize()
        {
            UserSession = HttpContext == null ? new UserSession() : new UserSession(HttpContext);
            UserRepo = new EFUserRepository();
            ProjectRepo = new EFProjectRepository();
        }

        public ActionResult Index2()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Timeline(Int32 prid,string filtername="All",string searchKeyword="")
        {
            ViewBag.UserPhoto = UserSession.UserInfoKeyAvatarPath;
            ProjectIssueModel vv = new ProjectIssueModel();
            vv.prdetail = ProjectRepo.GetProjectDetailByPrid(prid);
            if (filtername == "All")
            {
                if (searchKeyword == "")
                {
                    vv.IssueList = ProjectRepo.GetProjectIssueList(prid);
                }
                else
                {
                    var tt = ProjectRepo.GetProjectIssueList(prid);
                    vv.IssueList = tt.AsQueryable().Where(p => p.IssueDetail.Contains(searchKeyword)).ToList();

                }
            }
            else
            {
                if (searchKeyword == "")
                {

                    var tt = ProjectRepo.GetProjectIssueList(prid);
                    vv.IssueList = tt.AsQueryable().Where(p => p.IssuetypeId == Convert.ToInt16(filtername)).ToList();
                }
                else
                {
                    var tt = ProjectRepo.GetProjectIssueList(prid);
                    vv.IssueList = tt.AsQueryable().Where(p => p.IssueDetail.Contains(searchKeyword)).ToList();
                }
            }
            Session["ProjectID"] = prid;

         
            return View(vv);
        }

  
        [HttpGet]
        public ActionResult AccountList(string uid = "knothing.impossible@gmail.com")
        {
            string test = User.Identity.Name.ToString();
            AccountProject ap = new AccountProject();
            ap.userentity = UserRepo.GetUserEntitybyEmail(uid);
            ap.projectinhand = ProjectRepo.GetProjectDetailMobileByEmail(uid);
            return View(ap);
        }

        [HttpGet]
        public ActionResult Collaboration(Int32 prid)
        {
            ViewBag.prid = prid;
            CollaborationEntity en = new CollaborationEntity();
            en.prcollist = ProjectRepo.GetProjectCollabarationList(Convert.ToInt32(prid));
            List<IntStringObj> res = new List<IntStringObj>();
            //res.Add(new IntStringObj { number = 1, msg = "Non Result" });
            en.searchlist = res;
            return View(en);
        }


        [HttpPost]
        public ActionResult Collaboration(string searchkey, string prid)
        {
            CollaborationEntity en = new CollaborationEntity();
            if (searchkey != "")
            {

                en.prcollist = ProjectRepo.GetProjectCollabarationList(Convert.ToInt32(prid));
                en.searchlist = UserRepo.SearchUserNameOrEmail(searchkey);
                //var tt = UserRepo.SearchUserNameOrEmail(searchkey);
                //List<IntStringObj> res = tt.ToList();
                //var ee = UserRepo.SearchUserNameOrEmail(searchkey);
                //Int32 id = Convert.ToInt32(Request.QueryString["prid"]);
                // ViewBag.SearchList = UserRepo.SearchUserNameOrEmail(searchkey);
                ViewBag.prid = prid;
                //foreach (var rr in ViewBag.SearchList as List<BuikSiteWalkModel1.ModelEntities.IntStringObj>)
                //{
                //    ViewBag.message = @rr.msg;
                //    ViewBag.usernumber = @rr.number; 

                //}
                return View(en);

            }
            else
            {
                ViewBag.SearchList = null;
            }

            return View(en);
        }

        [HttpGet]
        public PartialViewResult CollaborationPartial()
        {


            return PartialView();
        }


        public ActionResult AddCollaboration(Int32 prid, Int32 userid)
        {
            Session["Userid"] = UserSession.UserInfoKeySurrKey;
            String projectIDAsString = prid.ToString();
            String UserIDAsString = userid.ToString();
            String sessionUserIDAsString = Session["Userid"].ToString();
           
            var rr = ProjectRepo.AddProjectCollaborationWithinvitedId(prid, userid, UserSession.UserInfoKeySurrKey, 1);

            String UsernameAsString = prepo.GetUNamebyId((Int32)Session["Userid"]);
            String ProjectNameAsString = prepo.GetProjectNamebyId(prid);

            var r2 = ProjectRepo.AddNotification(sessionUserIDAsString, UserIDAsString, 0, UsernameAsString + " invited you to join " + ProjectNameAsString + " project", "collaboration", projectIDAsString);
            return RedirectToAction("Collaboration", "SiteWalk", new { prid = prid});

        }


        [HttpGet]
        public ActionResult Index()
        {
            var tt = ProjectRepo.GetProjectDetailMobile(UserSession.UserInfoKeySurrKey);
            return View(tt);
        }

        //show the project detail and project issue list.
        [HttpGet]
        public ActionResult ProjectDetail(Int32 prid)
        {
            ProjectIssueModel vv = new ProjectIssueModel();
            vv.prdetail = ProjectRepo.GetProjectDetailByPrid(prid);
            vv.IssueList = ProjectRepo.GetProjectIssueList(prid);
            return View(vv);
        }


        [HttpGet]
        public ActionResult IssueDetail(Int32 issueid)
        {
            List<BuikIssueComment> tt = ProjectRepo.GetIssueCommentDetail2(issueid);
            return View(tt);
        }

        [HttpGet] //Knothing Added this to test
        public ActionResult GetIssueDetail(string issueId)
        {
            var res = ProjectRepo.GetProjectIssueListByIssueId(issueId);
            return View(res);
        }


        [HttpGet]
        public ActionResult IssueCommentDetail()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            List<string> ctrylist = ProjectRepo.GetProjectCountryList();
            ViewBag.ProjectTypes = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Resideltial House/Low-rise Building", Value = "1"},
                new SelectListItem(){ Text = "Condominium/High-rise Building", Value = "2"},
                new SelectListItem(){ Text = "Office/Commercial Building", Value = "3"},
                new SelectListItem(){ Text = "Road/Bridge, Infrastructure", Value = "4"},
                new SelectListItem(){ Text = "Other", Value = "5"}
            };
            ViewBag.Country = ctrylist.Select(x => new SelectListItem { Text = x, Value = x })
                 .ToList();
            return View();
        }
        //Int32 userid,string pname,string cname,string ptype,string pprovince,string pcountry,string psdate,string pedate

        [HttpPost]
        public ActionResult Create(ProjectModel p, HttpPostedFileBase image44)
        {
            Int32 uid = UserRepo.GetUserIdByEmail(User.Identity.Name);
            string r = p.ProjectName;
            string ff = p.StartDate.ToString();
            //AddNewProjectMobileWithLat(Int32 userid, string pname, string cname, string ptype, string pprovince, string pcountry, string psdate, string pedate,string lat,string longt)
            BuikProjectEntity pp = new BuikProjectEntity();
            pp.ProjectName = p.ProjectName;// pname;
            pp.CustomerName = p.CustomerName;// cname;
            pp.ProjectType = p.ProjectType.ToString();// ptype;
            pp.ProjectCountry = "THA";//p.Country.ToString();// pcountry;
            pp.ProjectProvince = p.Province;
            pp.ProjectStartDate = p.StartDate.ToShortDateString();
            pp.ProjectEndDate = p.EndDate.ToShortTimeString();
            //p.CreateUserName = userid;//Session["UserDisplayName"].ToString();
            pp.createuserid = uid;
            pp.latitude = Convert.ToDecimal(p.Lat);
            pp.longtitude = Convert.ToDecimal(p.Long);
            pp.ProjectTimelinePhoto = UploadPhotoCover(image44);
            string res = ProjectRepo.AddNewProjectMobileWithLat(pp);
            if (Convert.ToInt32(res) < 1)
            {
                //res = "Unable to Add Project Data";
                return RedirectToAction("Create", "SiteWalk");//Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Session["ProjectID"] = res;
                return RedirectToAction("Index", "SiteWalk");//, new { prid = en.ProjectSurrKey }));
            }
            //string gg = 

        }
        private string UploadPhotoCover(HttpPostedFileBase image)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("coverphoto");
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
            blob.UploadFromStream(image.InputStream);
            string uri = blob.Uri.ToString();
            return uri;
        }
        // Knothing Added here for create 'Project Setting'
        [HttpGet]
        public ActionResult ProjectSetting(Int32 prid)
        {
            ProjectIssueModel objectProjectIssue = new ProjectIssueModel();
            objectProjectIssue.prdetail = ProjectRepo.GetProjectDetailByPrid(prid);
            objectProjectIssue.IssueList = ProjectRepo.GetProjectIssueList(prid);
            objectProjectIssue.projectID = prid;
            return View(objectProjectIssue);
        }

        [HttpGet]
        public ActionResult CreateProject()
        {
            ViewBag.ProjectTypes = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Resideltial House/Low-rise Building", Value = "1"},
                new SelectListItem(){ Text = "Condominium/High-rise Building", Value = "2"},
                new SelectListItem(){ Text = "Office/Commercial Building", Value = "3"},
                new SelectListItem(){ Text = "Road/Bridge, Infrastructure", Value = "4"},
                new SelectListItem(){ Text = "Other", Value = "5"}
            };
            return View();
        }

        [HttpPost]
        public ActionResult CreateProject(ProjectModel p, HttpPostedFileBase image)
        {
            return View();
        }



        [HttpPost]
        public ActionResult UploadCoverPhoto(HttpPostedFileBase image)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("coverphoto");
            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
            blob.UploadFromStream(image.InputStream);
            string uri = blob.Uri.ToString();
            return PartialView("_PhotoUploaded", new List<UploadPhotoModel>() { new UploadPhotoModel() { PhotoUrl = uri } });
        }

        [HttpPost]
        public ActionResult UploadFloorPlan(HttpPostedFileBase[] images)
        {
            BlobStorageService blobStorageService = new BlobStorageService();
            CloudBlobContainer blobContainer = blobStorageService.GetCloudBlobContainer("floorplanphoto");
            var uploadPhotos = new List<UploadPhotoModel>();
            foreach (var img in images)
            {
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(img.FileName);
                blob.UploadFromStream(img.InputStream);
                string uri = blob.Uri.ToString();
                uploadPhotos.Add(new UploadPhotoModel() { PhotoUrl = uri });
            }
            return PartialView("_PhotoUploaded", uploadPhotos);
        }

        [HttpGet]
        public ActionResult CreateProjectIssue(Int32 prid)
        {
            ViewBag.IssueTypes = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Progress", Value = "1"},
                new SelectListItem(){ Text = "Action Required", Value = "2"},
                new SelectListItem(){ Text = "Good Job", Value = "3"},
                new SelectListItem(){ Text = "Request for Information", Value = "4"},
                new SelectListItem(){ Text = "Defect", Value = "5"}
            };
            ViewBag.ProjectId = prid;
            return View();
        }

        BlobStorageService _blobStorageService = new BlobStorageService();
        //[HttpPost]
        public string UploadIP(HttpPostedFileBase image, string issueid)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = ProjectRepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(issueid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);

                string res = irepo.UpdateProjectIssuePhoto(issueid, blob.Uri.ToString()).ToString();
                return blob.Uri.ToString();
            }

            return "Error Uploading";

        }

        public string UploadFP(HttpPostedFileBase image, string projectid, string fpname)
        {
            //string fpath = SavePhotoToStorage(photo, "FloorPlan" + projectid.ToString());
            //string res = ProjectRepo.UpdateProjectProfilePhoto(projectid, fpath);
            //return res;// Json(res, JsonRequestBehavior.AllowGet); 
            //string fpath = "";// = SavePhotoToStorage(photo, "CoverPhoto");
            if (image != null && image.ContentLength > 0)
            {
                CloudBlobContainer blobContainer = _blobStorageService.GetCloudBlobContainer(projectid);
                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(image.FileName);
                blob.UploadFromStream(image.InputStream);

                //string res = ProjectRepo.AddProjectFloorPlanPhoto(Convert.ToInt32(projectid), blob.Uri.ToString(), fpname);
                return blob.Uri.ToString();
            }

            return "Error Uploading";

        }

        [HttpPost]
        public ActionResult CreateProjectIssue(BuikIssueEntity en, HttpPostedFileBase image44)
        {
            //AddProjectIssue(Int32 projectid, int issuetype, string issuedetail,Int32 userid,int prog,string issuefile,string issuefloorplan,string deadline,string filetype)
            //var res = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId, en.IssueDetail,userid, issuefile, issuefloorplan,deadline,prog,filetype);
            Int32 uid = UserRepo.GetUserIdByEmail(User.Identity.Name);
            Int32 i = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId, en.IssueDetail, uid, en.IssueFile, en.IssueFloorPlanPhoto, en.IssueDeadline, en.IssueProgress, en.IssueFileType);

            string pathf = UploadIP(image44, i.ToString());
            irepo.UpdateProjectIssuePhoto(i.ToString(), pathf);
            return RedirectToAction("ProjectDetail", "Project", new { prid = en.ProjectSurrKey });
        }



        [HttpPost]
        public ActionResult AddIssueComment(Int32 issueId, string issuecomment)
        {
            Int32 uid = 0;
            if (Session["Userid"] != null)
            {
                uid = (Int32)Session["Userid"];
            }
            else { uid = UserRepo.GetUserIdByEmail(User.Identity.Name.ToString()); }
            Int32 prid = ProjectRepo.GetProjectIdFromIssue(issueId);
            Int32 icid = irepo.AddProjIssueComment(uid, issueId, issuecomment);
            //IssueDetail(Int32 issueid)
            return RedirectToAction("IssueDetail", "SiteWalk", new { issueid = issueId });
        }

        [HttpPost]
        public ActionResult IssueClickedLike(Int32 issueId)
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddProjectFloorPlan(Int32 prid)
        {
            List<IntStringObj> photolist = ProjectRepo.GetProjectFloorPlan(prid);
            ViewBag.Projectname = ProjectRepo.GetProjectDetailByPrid(prid).ProjectName;
            ViewBag.Prid = prid;
            FloorPlanPhotoModel pr = new FloorPlanPhotoModel();
            pr.photolist = photolist;
            pr.prid = prid;
            return View(pr);
        }
        [HttpPost]
        public ActionResult AddProjectFloorPlan(FloorPlanPhotoModel p, HttpPostedFileBase image33)
        {
            string filepath = UploadFP(image33, p.prid.ToString(), p.name);
            ProjectRepo.AddProjectFloorPlanPhoto(p.prid, filepath, p.name);
            return RedirectToAction("AddProjectFloorPlan", new { prid = p.prid });
        }

        [HttpGet]
        public PartialViewResult CreateProjectIssuePartial()//Int32 prid)
        {
            string projectsession = Session["ProjectID"].ToString();
            Int32 prid = Convert.ToInt32(projectsession);
           
            ViewBag.IssueTypes = new List<SelectListItem>()
            {
                new SelectListItem(){ Text = "Progress", Value = "1"},
                new SelectListItem(){ Text = "Action Required", Value = "2"},
                new SelectListItem(){ Text = "Good Job", Value = "3"},
                new SelectListItem(){ Text = "Request for Information", Value = "4"},
                new SelectListItem(){ Text = "Defect", Value = "5"}
            };
            ViewBag.ProjectId = prid;
            return PartialView();
        }

        [HttpGet]
        public PartialViewResult IssueCommentPartial(string issueId)
        {
            var res = ProjectRepo.GetProjectIssueListByIssueId(issueId);
    
            return PartialView(res);
        }

        [HttpPost]
        public ActionResult CreateProjectIssuePartial(BuikIssueEntity en, HttpPostedFileBase image44)
        {
            //AddProjectIssue(Int32 projectid, int issuetype, string issuedetail,Int32 userid,int prog,string issuefile,string issuefloorplan,string deadline,string filetype)
            //var res = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId, en.IssueDetail,userid, issuefile, issuefloorplan,deadline,prog,filetype);
        

            if (image44 != null)
            {
                en.IssueFileType = "Image";
                en.IssueFloorPlanPhoto = "(null)"; // This is for web because there is no pin floor plan photo yet 
                Int32 uid = UserRepo.GetUserIdByEmail(User.Identity.Name);
                Int32 i = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId, en.IssueDetail, uid, en.IssueFile, en.IssueFloorPlanPhoto, en.IssueDeadline, en.IssueProgress, en.IssueFileType);
                string pathf = UploadIP(image44, i.ToString());
                irepo.UpdateProjectIssuePhoto(i.ToString(), pathf);
            }
            else
            {
                en.IssueFileType = "Text";
                en.IssueFloorPlanPhoto = "(null)"; // This is for web because there is no pin floor plan photo yet 
                Int32 uid = UserRepo.GetUserIdByEmail(User.Identity.Name);
                Int32 i = irepo.AddProjectIssue(en.ProjectSurrKey, en.IssuetypeId, en.IssueDetail, uid, en.IssueFile, en.IssueFloorPlanPhoto, en.IssueDeadline, en.IssueProgress, en.IssueFileType);            

            }
            return RedirectToAction("Timeline", "SiteWalk", new { prid = en.ProjectSurrKey });
        }

       
    }
}
