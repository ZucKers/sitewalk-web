﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BuikSiteWalkModel1
{
    public class UserSession
    {
        private static string _userInfoKeySurrKey = "100";
        private static string _userInfoKeyFullName = "101";
        private static string _userInfoKeyEmail = "102";
        private static string _userInfoKeyCultureId = "103";
        private static string _userInfoKeyUserStatusId = "104";
        private static string _userInfoKeyAvatarPath = "106";
        private static string _userInfoKeyCountry = "107";
        private static string _userInfoKeyAvatar = "108";

        //userScore
        private static string _userScoreActivities = "109";

        //CompanyInfoKey: Session for current company(Basic information)
        private static string _companyInfoCompanySurrKey = "200";
        private static string _companyInfoCompanyName = "201";
        private static string _companyInfoCompanyCode = "202";
        private static string _companyInfoCompanyLogoFileName = "203";
 
        private static string _userReturnUrl = "300";
        private static string _userLastLogin = "301";
        private static string _userLastActive = "302";

        private HttpContextBase ContextBase;

        //Constructors
        public UserSession()
        {
            ContextBase = new HttpContextWrapper(HttpContext.Current);
        }

        public UserSession(HttpContextBase contextBase)
        {
            ContextBase = contextBase;
        }

        //Properties
        public virtual int UserInfoKeySurrKey
        {
            get
            {
                if (ContextBase.Session[_userInfoKeySurrKey] == null)
                { return 0; }
                else
                { return (int)ContextBase.Session[_userInfoKeySurrKey]; }
            }
            set
            { ContextBase.Session[_userInfoKeySurrKey] = value; }

        }

        public virtual string UserInfoKeyFullName
        {
            get
            {
                if (ContextBase.Session[_userInfoKeyFullName] == null)
                { return string.Empty; }
                else
                { return ContextBase.Session[_userInfoKeyFullName].ToString(); }
            }
            set
            { ContextBase.Session[_userInfoKeyFullName] = value; }

        }

        public string UserInfoKeyAvatarPath
        {
            get
            {
                if (ContextBase.Session[_userInfoKeyAvatarPath] == null)
                { return "https://builkchamaleonstorage.blob.core.windows.net/upload/avatar/NoPhotoUser2_50x50.jpg"; }
                else
                { return ContextBase.Session[_userInfoKeyAvatarPath].ToString(); }
            }
            set
            { ContextBase.Session[_userInfoKeyAvatarPath] = value; }

        }

        public virtual int CompanyInfoKeySurrKey
        {
            get
            {
                if (ContextBase.Session[_companyInfoCompanySurrKey] == null)
                { return 0; }
                else
                { return (int)ContextBase.Session[_companyInfoCompanySurrKey]; }
            }
            set
            { ContextBase.Session[_companyInfoCompanySurrKey] = value; }

        }

        public virtual string CompanyInfoKeyName
        {
            get
            {
                if (ContextBase.Session[_companyInfoCompanyName] == null)
                { return string.Empty; }
                else
                { return ContextBase.Session[_companyInfoCompanyName].ToString(); }
            }
            set
            { ContextBase.Session[_companyInfoCompanyName] = value; }

        }
    }
}
