﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.EFDataAccess;

namespace BuikSiteWalkModel1.Services
{
    public interface IUserService {
        void SetUserSession(T_USER user, UserSession session);
        void SetCompanySession(T_COMPANY company, UserSession session);
    }
    public class UserService: IUserService
    {
        public void SetUserSession(T_USER user, UserSession session){
            session.UserInfoKeySurrKey = user.user_surr_key;
            session.UserInfoKeyFullName = user.user_fullname;
            session.UserInfoKeyAvatarPath = "https://builkchamaleonstorage.blob.core.windows.net/upload/avatar/" + user.user_avatar_filename;
        }

        public void SetCompanySession(T_COMPANY company, UserSession session)
        {
            session.CompanyInfoKeySurrKey = company.company_surr_key;
            session.CompanyInfoKeyName = company.company_name;
        }
    }
}
