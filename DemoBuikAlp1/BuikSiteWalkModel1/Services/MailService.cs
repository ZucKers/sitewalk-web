﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BuikSiteWalkModel1.Services
{
    public interface IMailService
    {
        void SendErrorReport(ExceptionContext e);
    }
    public class MailService: IMailService
    {
        private SmtpClient GetSmptClient()
        {
            var client = new SmtpClient("smtp.sendgrid.net", 587)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("builk", "mairuusi")
            };
            return client;
        }

        #region Error Report
        public void SendErrorReport(ExceptionContext e)
        {
            var newThread = new Thread(DoSendErrorReport);
            newThread.CurrentCulture = Thread.CurrentThread.CurrentCulture;
            newThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
            newThread.Start(e);
        }

        private void DoSendErrorReport(object ee)
        {
            ExceptionContext e = (ExceptionContext)ee;
            MailMessage m = new MailMessage();
            m.From = new MailAddress("support@builk.com", "Error Reporter");

            m.To.Add("support@builk.com");
            m.CC.Add("cenixoft@live.com");

            m.Subject = "SiteWalk Error on " + TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTimeOffset.UtcNow, "SE Asia Standard Time");

            m.Body = "<h1>" + m.Subject + "</h1><br/>" +
            "<b>URL : </b>" + e.HttpContext.Request.Url.ToString() + " <b>Method : </b>" + e.HttpContext.Request.HttpMethod + "<br /><br />" +
            "<b>Browser : </b>" + e.HttpContext.Request.Browser.Browser + " <b>Version : </b>" + e.HttpContext.Request.Browser.MajorVersion + "<br /><br />" +
            "<b>Message : </b>" + e.Exception.Message + "<br /><br />" +
            "<b>Stack Trace : <br/></b>=> " + e.Exception.StackTrace.ToString().Replace(System.Environment.NewLine, "<br/>=> ") + "<br /><br />" +
            "<b>Params : </b> <br/>";

            foreach (var key in e.HttpContext.Request.Params.AllKeys)
            {
                m.Body += "=> " + key + " : " + e.HttpContext.Request.Params[key] + "<br/>";
            }

            //e.HttpContext.Request.
            m.IsBodyHtml = true;

            var client = GetSmptClient();
            try
            {
                client.SendAsync(m, m);
            }
            catch (SmtpException ex)
            {
                if (ex.StatusCode.Equals(SmtpStatusCode.InsufficientStorage))
                {
                    //Send again to ensure this email gets sent 
                    client.SendAsync(m, m);
                }
                else
                {
                    Thread.CurrentThread.Abort();
                }
            }
        }
        #endregion
    }
}
