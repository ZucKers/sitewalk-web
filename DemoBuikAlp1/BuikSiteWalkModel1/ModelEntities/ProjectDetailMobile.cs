﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class ProjectDetailMobile
    {
        public Int32 prid { get; set; }
        public string ProjectName { get; set; }
        public string StartDate { get; set; }
        public string FinishDate { get; set; }
        public int UserJoin { get; set; }
        public string ProjectCoverPhoto { get; set; }
        public int isFav { get; set; }
        public Int32 CreateuserId { get; set; }
        public string ProjCountry { get; set; }
        public string ProjProvince { get; set; }
        public string CustomerName { get; set; }
       // public string CompanyName { get; set; }
        public int DayRemain
        {
            get
            {
                try
                {
                    if (FinishDate.Equals("N/A") || StartDate.Equals("N/A")) { return 0; }
                    else
                    {
                        TimeSpan res = Convert.ToDateTime(FinishDate) - DateTime.Now;
                        return res.Days;
                    }
                }
                catch { return 0; }
            }
            set { this.DayRemain = value; }
        }

        public ProjectDetailMobile() { }
        public ProjectDetailMobile(Int32 prid, string name, string sdate,string fdate, int userCol,int isFav)
        {
            this.prid = prid; this.ProjectName = name; this.StartDate = sdate; this.FinishDate = fdate; this.UserJoin = userCol; this.isFav = isFav;
            if (fdate.Equals("N/A") || sdate.Equals("N/A")) { DayRemain = 0; }
            else
            {
                TimeSpan res = Convert.ToDateTime(fdate) - DateTime.Now;
                DayRemain = res.Days;
            }
        }
    }
}
