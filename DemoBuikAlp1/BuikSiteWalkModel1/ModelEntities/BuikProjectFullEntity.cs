﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikProjectFullEntity : BuikProjectEntity
    {
        //public string ProjectCode { get; set; }
        public string ProjectProvince { get; set; }
        public string ProjectOwnerLocation { get; set; }
        public string ProjectOwner { get; set; }
        public float ProjectContractPrice { get; set; }

        public BuikProjectFullEntity() 
        {
            
        }

    }
}
