﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class IntStringObj
    {
        public IntStringObj() { }
        public IntStringObj(int n, string m)
        {
            this.number = n; this.msg = m;
        }
        public Int32 number { get; set; }
        public string msg { get; set; }
        public string det { get; set; }
    }
}
