﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikProjectCollaborationEntity
    {
        public Int32 ProjectId { get; set; }
        public Int32 InviterId { get; set; }
        public Int32 InvitedUserid { get; set; }
        public int InviteStatus { get; set; }
        public Int32 ProjectCollaborationId { get; set; }
        public int IsFavorite { get; set; }
    }
}
