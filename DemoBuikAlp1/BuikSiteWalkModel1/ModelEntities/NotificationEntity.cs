﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class NotificationEntity
    {
        public Int32 Id { get; set; }
        public string fromUID { get; set; }
        public string toUID { get; set; }
        public int IsR { get; set; }
        public string Msg { get; set; }
        public string NotType { get; set; }
        public string DestId { get; set; }
        public string Timestamp { get; set; }
    }
}
