﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikIssueEntity
    {
        public Int32 IssueId{get;set;}
        public Int32 ProjectSurrKey{get;set;}
        public int IssuetypeId{get;set;}
        public string IssueDetail{get;set;}
        public string IssueCreatorName{get;set;}
        public string IssueCreatorPhoto { get; set; }
        public string IssueDateTimeCreate{get;set;}
        public int IssueProgress{get;set;}
        public string IssueFloorPlanPhoto{get;set;}
        public string IssueFile{get;set;}
        public int IssueLike { get; set; }
        public int IssueComment { get; set; }
        public string IssueFileType { get; set; }
        public string IssueDeadline { get; set; }
        public int IsPersonINC { get; set; }
        public string IssueCreatCompany { get; set; }
        public Int32 IssueCreatorId { get; set; }
        public string IsTypeName { get; set; }
    }
}
