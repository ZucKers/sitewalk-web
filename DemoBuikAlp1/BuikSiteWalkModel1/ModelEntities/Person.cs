﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class Person
    {
        string FirstName;
        string LastName;
        public Person(string fn, string ln) { FirstName = fn; LastName = ln; }
    }

    public class Team
    {
        string TeamName;
        Person TeamLeader;
        List<Person> TeamMembers;

        public Team(string name, Person lead, List<Person> members)
        {
            TeamName = name;
            TeamLeader = lead;
            TeamMembers = members;
        }
    }

    public class Response
    {
        int ResponseCode;
        string ResponseMessage;
        object ResponsePayload;
        public Response(int code, string message, object payload)
        {
            ResponseCode = code;
            ResponseMessage = message;
            ResponsePayload = payload;
        }
    }
}
