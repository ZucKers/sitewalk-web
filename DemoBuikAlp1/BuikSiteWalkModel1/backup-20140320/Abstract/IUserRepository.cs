﻿using System;
using System.Collections.Generic;
using BuikSiteWalkModel1.EFDataAccess;
using BuikSiteWalkModel1.ModelEntities;
namespace BuikSiteWalkModel1.Abstract
{
    public interface IUserRepository
    {
        Int32 AddNewRegisterUser(BuikSiteWalkModel1.ModelEntities.BuikUserEntity user);
        BuikSiteWalkModel1.ModelEntities.BuikUserEntity GetUserEntitybyEmail(string email);
        List<IntStringObj> SearchUserName(string name);
        List<IntStringObj> SearchUserNameOrEmail(string nameoremail);
        List<IntStringObj> SearchUserByEmail(string email);
        int GetUserIdByEmail(string usermail);
        BuikUserEntity GetUserEntitybyId(Int32 uid);
        int UpdateUserDetail(Int32 userid, string userName,string usersname, string companyname, string btypeid);
        int UpdateuserPhoto(string filepath, Int32 userid);
        int AddUserInvitation(string email, Int32 uid, Int32 prid, string tokenid);
        int UpdateUserInvitation(Int32 invtid);
        void ScanAndUpdateInvitation(string useremail, Int32 userid);
        Int32 SearchUserEmail(string email);
        T_USER GetUserBySurrogateKey(int id);
    }
}
