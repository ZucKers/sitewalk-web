//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BuikSiteWalkModel1.EFDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Project_Collaboration
    {
        public int proj_collaboration_id { get; set; }
        public int proj_surr_key { get; set; }
        public int user_surr_key { get; set; }
        public int inviter_surr_key { get; set; }
        public int invitation_status { get; set; }
        public int isFav { get; set; }
        public Nullable<int> IsShow { get; set; }
    }
}
