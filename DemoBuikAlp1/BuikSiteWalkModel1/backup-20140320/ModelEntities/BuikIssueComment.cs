﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikIssueComment
    {
        public string CUserName { get; set; }
        public string CommentD { get; set; }
        public string CDatetime { get; set; }
        public Int32 CUid { get; set; }
        public string CUPhoto { get; set; }
        public Int32 CommentIssueId { get; set; }
    }
}
