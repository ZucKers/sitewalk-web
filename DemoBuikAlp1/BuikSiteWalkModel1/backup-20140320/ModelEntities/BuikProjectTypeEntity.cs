﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikProjectTypeEntity
    {
        public BuikProjectTypeEntity() { }
        public BuikProjectTypeEntity(int pp,string id,string cultid,string name)
        {
            this.ProjectTypeId = id;this.ProjectTypeCultureId = cultid;
            this.ProjectTypeName = name; this.pkid = pp;
        }
        public int pkid { get; set; }
        public string ProjectTypeId { get; set; }
        public string ProjectTypeCultureId { get; set; }
        public string ProjectTypeName { get; set; }
    }
}
