﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikUserEntity
    {
        public string UserId { get; set; }
        public string UserEmail { get; set; }
        //private string UserFullName;
        public string UserFullName { get; set; }
        public string userPhoto { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserCountry { get; set; }
        public string UserCompamy { get; set; }
        public string UserBusinessType { get; set; }
        public string CreateSince { get; set; }
    }
}
