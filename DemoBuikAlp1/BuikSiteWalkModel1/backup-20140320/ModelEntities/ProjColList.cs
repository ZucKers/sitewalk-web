﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class ProjColList
    {
        //public ProjColList(Int32 uid, string uname, int stat)
        //{
        //    this.userid = uid; this.username = uname;
        //    if (stat == 1) { this.invitestatus = "Pending"; }
        //    else if (stat == 2){this.invitestatus = "Accepted";}
        //    else this.invitestatus= "Unknown";
        //}
        public Int32 userid { get; set; }
        public string username { get; set; }
        public string invitestatus { get; set; }
        public int inv { get; set; }
        public string memberstatus { get; set; }
        public void Setinvitedstatus() 
        {
             if (inv == 1) { this.invitestatus = "Pending"; }
           else if (inv == 2){this.invitestatus = "Accepted";}
            else this.invitestatus= "Unknown";
        }
    }
}
