﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuikSiteWalkModel1.ModelEntities
{
    public class BuikProjectEntity
    {
        public Int32 prid { get; set; }
        public Int32 createuserid { get; set; }
        public string CreateUserName { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string CustomerName { get; set; }
        public string ProjectType { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectProvince { get; set; }
        public string ProjectCountry { get; set; }
        public string ProjectStartDate { get; set; }
        public string ProjectEndDate { get; set; }
        public string ProjectTimelinePhoto { get; set; }
        public int IsFav { get; set; }
        public decimal latitude { get; set; }
        public decimal longtitude { get; set; }
        private List<string> ProjectFloorPlan;
        public List<string> GetAllProjectFloorPlan()
        { return ProjectFloorPlan; }
        public void AddProjectFloorPlan(string pa)
        { this.ProjectFloorPlan.Add(pa); }
        public string GetFloorPlan(int i)
        {
            return ProjectFloorPlan[i];
        }
        public int isshow { get; set; }
        public BuikProjectEntity() { }

        public BuikProjectEntity(Int32 prid,Int32 userid,string code,string name,string cusname,string typ,string location,string startdate,string enddate,string photo)
        {
            this.prid = prid; this.createuserid = userid;
            this.ProjectCode = code; this.ProjectName = name; this.CustomerName = cusname; this.ProjectType = typ;
            this.ProjectLocation = location;this.ProjectStartDate = startdate;this.ProjectEndDate = enddate;this.ProjectTimelinePhoto = photo;
        }

        public BuikProjectEntity(Int32 prid, Int32 userid,string code,string name, string cusname,string typ, string location, string startdate, string enddate, string photo,List<string> fp)
        {
            //this.BuikProjectEntity(prid,code,name,cusname,typ,location,startdate,enddate,photo);
            this.prid = prid; this.createuserid = userid;
            this.ProjectCode = code; this.ProjectName = name; this.CustomerName = cusname; this.ProjectType = typ;
            this.ProjectLocation = location; this.ProjectStartDate = startdate; this.ProjectEndDate = enddate; this.ProjectTimelinePhoto = photo;
            this.ProjectFloorPlan = fp;
        }

        public String DayToGo()
        {
            DateTime d = Convert.ToDateTime(ProjectEndDate);
            
            return (d - DateTime.Today).TotalDays.ToString();
        }
    }
}
