﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.Abstract;
using BuikSiteWalkModel1.ModelEntities;

namespace BuikSiteWalkModel1.Repository
{
    public class FakeProjectRepository //: IProjectRepository
    {
        public List<BuikIssueEntity> GetProjectIssueList(Int32 prid)
        {
            List<BuikIssueEntity> res = new List<BuikIssueEntity>();
            for(int i = 0;i< 60 ;i++)
            {
                BuikIssueEntity r1 = new BuikIssueEntity();//{
                r1.IssueId =  999+i;
                r1.IssuetypeId = 1;
                r1.IssueDetail = "TestIssueDetail for maximum issue list no "+ i;
                r1.IssueDateTimeCreate = DateTime.Now.ToShortDateString();
                r1.IssueCreatorName ="Tess Script 01";
                r1.IssueCreatCompany="Cenix Soft test1";
                r1.IssueComment = i;
                r1.IssueDeadline = DateTime.Now.ToShortDateString();
                r1.IssueFileType = "Text";
                r1.IssueProgress = i;
                r1.ProjectSurrKey = 99;
                r1.IssueLike = i;
                r1.IsPersonINC = 3;//,"","","","",2+ i,i, DateTime.Now.ToShortDateString(),"","text",i,"Cenix Soft",2013009
                res.Add(r1);                  
                    };
            return res;
        }
          
           
        
    
        
        public List<BuikProjectTypeEntity> GetAllProjectType()
        {
            List<BuikProjectTypeEntity> li = new List<BuikProjectTypeEntity>();
            li.Add(new BuikProjectTypeEntity(1,"0100","1033","Residential House / Low Rise Building"));
            li.Add(new BuikProjectTypeEntity(2,"0200", "1033", "Condominium / High Rise Building"));
            li.Add(new BuikProjectTypeEntity(3,"0300", "1033", "Office or Commercial Building"));
            li.Add(new BuikProjectTypeEntity(4,"0400", "1033", "Factory / Warehouse"));
            li.Add(new BuikProjectTypeEntity(5,"0500", "1033", "Road / Bridge / Infrastructure"));
            li.Add(new BuikProjectTypeEntity(6,"0100", "1054", "บ้าน/ทาวน์เฮ้าส์/ที่พักอาศัยแนวราบ"));
            li.Add(new BuikProjectTypeEntity(7, "0200", "1054", "หอพัก/คอนโดมิเนียม/ที่พักอาศัยแนวสูง"));
            li.Add(new BuikProjectTypeEntity(8, "0300", "1054", "อาคารสำนักงาน ศูนย์การค้า อาคารสาธารณะ"));
            li.Add(new BuikProjectTypeEntity(9, "0400", "1054", "โรงงาน โกดัง"));
            li.Add(new BuikProjectTypeEntity(10, "0500", "1054", "ถนน สะพาน สาธารณูปโภค"));
            li.Add(new BuikProjectTypeEntity(11, "0600", "1033", "Hospital"));
            li.Add(new BuikProjectTypeEntity(12, "0600", "1054", "โรงพยาบาล"));
            li.Add(new BuikProjectTypeEntity(13, "0700", "1033", "Other"));
            li.Add(new BuikProjectTypeEntity(14, "0700", "1054", "อื่นๆ"));
            li.Add(new BuikProjectTypeEntity(15, "0100", "1057", "Rumah Tinggal / Bangunan Rendah"));
            li.Add(new BuikProjectTypeEntity(16,"0200", "1057", "Kondominium / Bangunan Tinggi"));
            li.Add(new BuikProjectTypeEntity(17, "0300", "1057", "Kantor atau bangunan komersial"));
            li.Add(new BuikProjectTypeEntity(18, "0400", "1057", "Pabrik / Gudang"));
            li.Add(new BuikProjectTypeEntity(19, "0500", "1057", "Jalan / Jembatan / Infrastruktur"));
            li.Add(new BuikProjectTypeEntity(20, "0600", "1057", "Rumah Sakit"));
            li.Add(new BuikProjectTypeEntity(21, "0700", "1057", "Lain-Lain"));
            return li;
        }

        public void AddNewProjectMobile(BuikProjectEntity pr)
        {
            throw new NotImplementedException();
        }

        public void AddProjectCollaboration(string prid, string userid)
        {
            throw new NotImplementedException();
        }

        public void EditProjectMobile(BuikProjectEntity pr1)
        {
            throw new NotImplementedException();
        }

        public List<BuikProjectEntity> GetProjectMobileEntityByUser(int id)
        {
            throw new NotImplementedException();
        }

        public List<BuikProjectTypeEntity> GetProjectTypeBCulture(string cid)
        {
            throw new NotImplementedException();
        }

        public void RmoveProjectCollaboration(string prid, string userid)
        {
            throw new NotImplementedException();
        }


        public List<BuikProjectEntity> GetProjectMobileEntityByCollaborationId(string username)
        {
            throw new NotImplementedException();
        }


        public void RemoveProjectCollaboration(string prid, string userid)
        {
            throw new NotImplementedException();
        }


        public void RemoveProjectCollaboration(string prid, string userid, string inviter)
        {
            throw new NotImplementedException();
        }
    }
}
