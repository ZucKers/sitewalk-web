﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.Abstract;
//using BuikSiteWalkModel1.EFRepository;
using BuikSiteWalkModel1.ModelEntities;
using System.Data.Entity;
using BuikSiteWalkModel1.EFDataAccess;
using System.Data.Objects.SqlClient;
using System.Configuration;
using System.Globalization;

namespace BuikSiteWalkModel1.Repository
{
    public class EFProjectRepository : BuikSiteWalkModel1.Abstract.IProjectRepository
    {
        string fpath = "https://builkchamaleonstorage.blob.core.windows.net/upload/avatar/";

        public List<BuikProjectTypeEntity> GetAllProjectType()
        {
            using(var db = new BuilkChameleonEntities())
            {
                var query = from b in db.T_PROJECT_TYPE
                            orderby b.projtype_display_order
                            select             
                            new BuikProjectTypeEntity
                            {
                                pkid = b.projtype_surr_key,
                                ProjectTypeId = b.projtype_id,
                                ProjectTypeCultureId = b.projtype_culture_id,
                                ProjectTypeName = b.projtype_name
                            };
                
                return query.ToList();
            }
           
        }

        public List<BuikProjectTypeEntity> GetProjectTypeBCulture(string cid)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var query = from b in db.T_PROJECT_TYPE
                            where b.projtype_culture_id == cid
                            orderby b.projtype_display_order
                            select
                            new BuikProjectTypeEntity
                            {
                                pkid = b.projtype_surr_key,
                                ProjectTypeId = b.projtype_id,
                                ProjectTypeCultureId = b.projtype_culture_id,
                                ProjectTypeName = b.projtype_name
                            };

                return query.ToList();
            }
        }

        public int IsProjecttIsFav(int prid,int invitedid)
        {
            try
            {
                var g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == prid && p.user_surr_key == invitedid).Select(q => q.isFav).SingleOrDefault();
                return g;
            }
            catch { return 0; }
            
        }

        public int SetProjAsFav(Int32 userid, Int32 prid,int s)
        {
            int res = 0;
            try { 
                string sql1 = "update Project_Collaboration set isFav = "+s+" where user_surr_key = "+userid+" and proj_surr_key = "+prid+"";
                res = new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
            }
            catch{res= 0;}
            return res;
        }

        public List<BuikProjectEntity> SortedByFav(List<BuikProjectEntity> prList,Int32 userid)
        {
            foreach (BuikProjectEntity pe in prList)
            {
                pe.IsFav = IsProjecttIsFav(Convert.ToInt32(pe.prid), userid);
            }
            return prList.OrderByDescending(p => p.IsFav).OrderBy(p => p.ProjectStartDate).ToList();
        }
        public List<BuikProjectEntity> GetProjectMobileEntityByCollaborationIdEmail(string useremail)
        {
            return GetProjectMobileEntityByCollaborationId(this.GetUserIdByEmail(useremail));
        }
        public List<BuikProjectEntity> GetProjectMobileEntityByCollaborationId2(Int32 id)
        {
            List<BuikProjectEntity> res = new List<BuikProjectEntity>();
            var g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == id && p.invitation_status == 2).Select(q => q.proj_surr_key).ToArray();
            var dd = new BuilkChameleonEntities().T_PROJECT.Where(p => g.Contains(p.proj_surr_key)).ToList();
            foreach (T_PROJECT bb in dd)
            {                             
                BuikProjectEntity pr = new BuikProjectEntity(bb.proj_surr_key, bb.create_user_id, bb.proj_code, bb.proj_name, bb.proj_owner, bb.projtype_id, bb.proj_location, bb.proj_start_date.ToString(), bb.proj_finish_date.ToString(), bb.proj_photo_filename);
                res.Add(pr);
                           }
            return res;          
        }

        public List<BuikProjectEntity> GetProjectMobileEntityByCollaborationId(Int32 id)
        {
            List<BuikProjectEntity> res = new List<BuikProjectEntity>();
            using (var db = new BuilkChameleonEntities())
            {
                var gg = (from t in db.T_PROJECT
                          join p in db.Project_Collaboration
                          on t.proj_surr_key equals p.proj_surr_key into g
                          from g1 in g.Where(p => p.user_surr_key == id && p.invitation_status == 2 && p.IsShow == 1)
                          select new {
                            prid = g1.proj_surr_key,
                            cuserid = t.create_user_id,
                            proj_code = t.proj_code,
                            proj_name = t.proj_name,
                            proj_owner = t.proj_owner,
                            proj_type = t.projtype_id,
                            proj_location = t.proj_location,
                            start_date = t.proj_start_date,
                            finish_date = t.proj_finish_date,
                            photo_f = t.proj_photo_filename
                          }).ToList().Select(kk => new BuikProjectEntity
                          {
                            prid = kk.prid,
                            createuserid = kk.cuserid,
                            ProjectCode = kk.proj_code,
                            ProjectName = kk.proj_name,
                            CustomerName = kk.proj_owner,
                            ProjectType = kk.proj_type,
                            ProjectLocation = kk.proj_location,
                            ProjectStartDate = kk.start_date.HasValue ? kk.start_date.Value.ToString("MM/dd/yyyy") : "N/A",
                            ProjectEndDate = kk.finish_date.HasValue ? kk.finish_date.Value.ToString("MM/dd/yyyy") : "N/A",
                            ProjectTimelinePhoto = kk.photo_f
                          }).ToList();
                return gg;              
                              
            }
            
        }

        public List<BuikProjectEntity> GetProjectMobileEntityByCollaborationIdShowAll(Int32 id)
        {
            List<BuikProjectEntity> res = new List<BuikProjectEntity>();
            using (var db = new BuilkChameleonEntities())
            {
                var gg = (from t in db.T_PROJECT
                          join p in db.Project_Collaboration
                          on t.proj_surr_key equals p.proj_surr_key into g
                          from g1 in g.Where(p => p.user_surr_key == id && p.invitation_status == 2)
                          select new
                          {
                              prid = g1.proj_surr_key,
                              cuserid = t.create_user_id,
                              proj_code = t.proj_code,
                              proj_name = t.proj_name,
                              proj_owner = t.proj_owner,
                              proj_type = t.projtype_id,
                              proj_location = t.proj_location,
                              start_date = t.proj_start_date,
                              finish_date = t.proj_finish_date,
                              photo_f = t.proj_photo_filename,
                              IsShow = g1.IsShow
                          }).ToList().Select(kk => new BuikProjectEntity
                          {
                              prid = kk.prid,
                              createuserid = kk.cuserid,
                              ProjectCode = kk.proj_code,
                              ProjectName = kk.proj_name,
                              CustomerName = kk.proj_owner,
                              ProjectType = kk.proj_type,
                              ProjectLocation = kk.proj_location,
                              ProjectStartDate = kk.start_date.HasValue ? kk.start_date.Value.ToString("MM/dd/yyyy") : "N/A",
                              ProjectEndDate = kk.finish_date.HasValue ? kk.finish_date.Value.ToString("MM/dd/yyyy") : "N/A",
                              ProjectTimelinePhoto = kk.photo_f,
                              isshow = kk.IsShow.HasValue ? (int)kk.IsShow : 0
                          }).ToList();
                return gg;

            }

        }

        public List<ProjectDetailMobile> GetProjectDetailMobile2(Int32 id)
        {
            List<ProjectDetailMobile> res = new List<ProjectDetailMobile>();
            //Int32[] g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == id && p.invitation_status == 2).Select(q => q.proj_surr_key).ToArray();
            ////var g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.inviter_surr_key == id)
            ////BuilkChameleonEntities bb = new BuilkChameleonEntities();
            //var dd = new BuilkChameleonEntities().T_PROJECT.Where(p => g.Contains(p.proj_surr_key));
            //foreach (T_PROJECT tt in dd)
            //{
            //    int nu = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == tt.proj_surr_key && p.invitation_status == 2).Count();
            //    int isf = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == tt.proj_surr_key && p.invitation_status == 2 && p.user_surr_key == id).Select( oo => oo.isFav).FirstOrDefault();
            //    ProjectDetailMobile r = new ProjectDetailMobile(tt.proj_surr_key, tt.proj_name, tt.proj_start_date.Value.ToShortDateString(),tt.proj_finish_date.Value.ToShortDateString(), nu,isf);
            //    r.CreateuserId = id;
            //    r.ProjectCoverPhoto = tt.proj_photo_filename;
            //    r.CustomerName = tt.proj_owner;
            //    r.ProjProvince = tt.proj_province;
            //    r.ProjCountry = tt.proj_country;
            //    res.Add(r);
            //}            
            var d = new BuilkChameleonEntities();
            var dd = from t in d.Project_Collaboration
                     where t.user_surr_key.Equals(id) && t.invitation_status.Equals(2)
                     join q in d.T_PROJECT on t.proj_surr_key equals q.proj_surr_key
                    group t by t.proj_surr_key into h
                    // select new {kk = h.Key, pcount = h.Count()};
                     
                     select new ProjectDetailMobile
                     {
                         prid = h.Key,
                         //FinishDate = h.
                         //ProjectName = ,
                         //StartDate = q.proj_start_date,//.HasValue ? ( : "N/A",
                         //FinishDate = q.proj_finish_date,//.HasValue ? q.proj_finish_date.ToString() : "N/A",
                         //isFav = t.isFav,
                         //CreateuserId = id,
                         //ProjectCoverPhoto = q.proj_photo_filename,
                         //CustomerName = q.proj_owner,
                         //ProjCountry = q.proj_owner_country,
                         //ProjProvince = q.proj_owner_province
                        //UserJoin= h.Count()
                     };
            dd.ToList();
            return res;//dd.ToList<ProjectDetailMobile>();//res.OrderBy(p => p.DayRemain).AsEnumerable().ToList();
        }
        public List<ProjectDetailMobile> GetProjectDetailMobile3(Int32 id)
        {
            List<ProjectDetailMobile> res = new List<ProjectDetailMobile>();
            Int32[] g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == id && p.invitation_status == 2).Select(q => q.proj_surr_key).ToArray();
            //var g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.inviter_surr_key == id)
            //BuilkChameleonEntities bb = new BuilkChameleonEntities();
            var dd = new BuilkChameleonEntities().T_PROJECT.Where(p => g.Contains(p.proj_surr_key));
            foreach (T_PROJECT tt in dd)
            {
                int nu = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == tt.proj_surr_key && p.invitation_status == 2).Count();
                int isf = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == tt.proj_surr_key && p.invitation_status == 2 && p.user_surr_key == id).Select(oo => oo.isFav).FirstOrDefault();
                ProjectDetailMobile r = new ProjectDetailMobile(tt.proj_surr_key, tt.proj_name, tt.proj_start_date.HasValue? tt.proj_start_date.Value.ToShortDateString(): "N/A",tt.proj_finish_date.HasValue? tt.proj_finish_date.Value.ToShortDateString():"N/A", nu, isf);
                r.CreateuserId = id;
                r.ProjectCoverPhoto = tt.proj_photo_filename;
                r.CustomerName = tt.proj_owner;
                r.ProjProvince = tt.proj_province;
                r.ProjCountry = tt.proj_country;
                res.Add(r);
            }
            return res.OrderBy(p => p.DayRemain).AsEnumerable().ToList();
        }
        public List<ProjectDetailMobile> GetProjectDetailMobile(Int32 id)
        {
            ProjectDetailMobile res = new ProjectDetailMobile();
            var db = new BuilkChameleonEntities();
            var gg = (from t in db.T_PROJECT                   
                     join p in db.Project_Collaboration
                     on t.proj_surr_key equals p.proj_surr_key into g                  
                     from g1 in g.Where(p => p.user_surr_key == id && p.invitation_status == 2 && p.IsShow == 1)                    
                     select new 
                     {
                         prid = t.proj_surr_key,
                         CreateuserId = t.create_user_id,
                         CustomerName = t.proj_owner,
                         ProjectName = t.proj_name,
                         ProjProvince = t.proj_province,
                         ProjCountry = t.proj_country,
                         ProjectCoverPhoto = t.proj_photo_filename,
                         isFav = g.FirstOrDefault().isFav,
                         StartDate = t.proj_start_date,
                         EndDate = t.proj_finish_date,
                         UserJoin = g.Count()
                     }).ToList().Select(uu => new ProjectDetailMobile{
                       prid = uu.prid,
                       CreateuserId = uu.CreateuserId,
                       CustomerName = uu.CustomerName,
                       ProjectName = uu.ProjectName,
                       ProjProvince = uu.ProjProvince,
                       ProjCountry = uu.ProjCountry,
                       ProjectCoverPhoto = string.IsNullOrWhiteSpace(uu.ProjectCoverPhoto) ? "https://builkchamaleonstorage.blob.core.windows.net/content/Style/images/defaultCover.jpg" : uu.ProjectCoverPhoto,
                       isFav = this.GetIsFavList(uu.prid,id),
                       StartDate = uu.StartDate.HasValue ? uu.StartDate.Value.ToString("MM/dd/yyyy") : "N/A",
                       FinishDate = uu.EndDate.HasValue ? uu.EndDate.Value.ToString("MM/dd/yyyy") : "N/A",
                       UserJoin = uu.UserJoin
                     });
            return gg.ToList();
        }

        public int GetIsFavList(Int32 prid,Int32 userid)
        {          
            var db = new BuilkChameleonEntities();
            var res = db.Project_Collaboration.Where(p => p.user_surr_key == userid && p.proj_surr_key == prid).Select(q => q.isFav).FirstOrDefault();
            return res;
        }

        public List<ProjectDetailMobile> GetProjectDetailMobileByEmail(string email)
        {
            return GetProjectDetailMobile(GetUserIdByEmail(email));
        }

        public ProjectDetailMobile GetProjectDetailByPrid(Int32 prid)
        {
            ProjectDetailMobile res = new ProjectDetailMobile();
            //Int32[] g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == id && p.invitation_status == 2).Select(q => q.proj_surr_key).ToArray();
            //var g = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.inviter_surr_key == id)
            //BuilkChameleonEntities bb = new BuilkChameleonEntities();
            var db = new BuilkChameleonEntities();
            var gg = (from t in db.T_PROJECT
                     where t.proj_surr_key.Equals(prid)
                     join p in db.Project_Collaboration on t.proj_surr_key equals p.proj_surr_key into g
                    
                     select new {
                        prid = t.proj_surr_key,
                        CreateuserId = t.create_user_id,//.create_user_id,
                        CustomerName = t.proj_owner,
                        ProjectName = t.proj_name,
                        ProjProvince = t.proj_province,
                        ProjCountry = t.proj_country,
                        ProjectCoverPhoto = t.proj_photo_filename,
                        isFav = g.FirstOrDefault().isFav,
                        UserJoin = g.Count(),
                        Sdate = t.proj_start_date,
                        FDate = t.proj_finish_date
                     }).ToList().Select(uu => new ProjectDetailMobile
                     {
                        prid = uu.prid,
                       CreateuserId = uu.CreateuserId,
                       CustomerName = uu.CustomerName,
                       ProjectName = uu.ProjectName,
                       ProjProvince = uu.ProjProvince,
                       ProjCountry = uu.ProjCountry,
                       ProjectCoverPhoto = uu.ProjectCoverPhoto,
                       isFav = uu.isFav,
                       StartDate = uu.Sdate.HasValue ? uu.Sdate.Value.ToShortDateString() : "N/A",
                       FinishDate = uu.FDate.HasValue ? uu.FDate.Value.ToShortDateString() : "N/A",
                       UserJoin = uu.UserJoin
                     });

           
            
            return gg.FirstOrDefault();
        }

        public int GetIssueLikeCount2Unused(Int32 issueid)
        {
            return new BuilkChameleonEntities().IssueLikes.Where(p => p.IssueId == issueid).Count();
        }
        public int GetIssueLikeCount(Int32 issueid)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var rr = (from Il in db.IssueLikes
                          where Il.IssueId == issueid
                          select Il).Count();

                return rr;
            }
            
        }
        public int GetIssueCommentCount2(Int32 issueid)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var rr = (from Il in db.IssueComments
                          where Il.IssueId == issueid
                          select Il).Count();

                return rr;
            }

        }
        public int GetIssueCommentCount(Int32 issueid)
        {
            return new BuilkChameleonEntities().IssueComments.Where(p => p.IssueId == issueid).Count();

        }
        public List<BuikIssueEntity> GetProjectIssueListOld88(Int32 prid)
        {
            //from t in db.T_PROJECT
            //              join p in db.Project_Collaboration
            //              on t.proj_surr_key equals p.proj_surr_key into g
            //              from g1 in g.Where(p => p.user_surr_key == id && p.invitation_status == 2)
            List<BuikIssueEntity> res = new List<BuikIssueEntity>();
            var dd = new BuilkChameleonEntities().Project_Issue.Where(p => p.proj_surr_key == prid);
            var db = new BuilkChameleonEntities();
            var dd2 = (from p in db.Project_Issue
                       join t in db.T_USER
                       on p.IssueCreator equals t.user_surr_key into g
                       where p.proj_surr_key == prid
                       from g1 in g
                       select new
                       {
                           id = p.IssueID,
                           projId = p.proj_surr_key,
                           typeid = p.IssueTypeId,
                           dtail = p.IssueDetail,
                           dtcreate = p.IssueDateTimeCreate,
                           creatorname = g1.user_fullname,
                           cretorphoto = fpath + g1.user_avatar_filename,
                           issueprogress = p.IssueProgress,
                           fpphoto = String.IsNullOrEmpty(p.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : p.IssueFloorPlanPhoto,
                           issuefile = p.IssueFile,
                           ideadline = p.IssueDeadline,
                           filetype = p.IssueFileType,
                           comp = g1.user_company
                       }
                               ).ToList().Select(uu => new BuikIssueEntity 
                               {
                                   IssueId = Convert.ToInt32(uu.id),
                                   IssuetypeId = uu.typeid,//r1.IssueTypeId,
                                   IssueDetail = uu.dtail,//r1.IssueDetail,
                                   IssueDateTimeCreate = uu.dtcreate,//r1.IssueDateTimeCreate,
                                   IssueCreatorName = uu.creatorname,//GetUserFullNamebyId(r1.IssueCreator),//r2.user_fullname,
                                   IssueCreatorPhoto = uu.cretorphoto,//fpath + GetUserPhotobyId(r1.IssueCreator),
                                   IssueProgress = (int)uu.issueprogress,//Convert.ToInt16(r1.IssueProgress),
                                   //IssueFloorPlanPhoto = AddFpath(r1.IssueFloorPlanPhoto),
                                   IssueFloorPlanPhoto = uu.fpphoto,//String.IsNullOrEmpty(r1.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : r1.IssueFloorPlanPhoto,
                                   IssueFile = uu.issuefile,//r1.IssueFile,
                                   IssueComment = GetIssueCommentCount(Convert.ToInt32(uu.id)),
                                   IssueLike = GetIssueLikeCount(Convert.ToInt32(uu.id)),
                                   IssueDeadline = uu.ideadline,
                                   IssueFileType = uu.filetype,
                                   IsPersonINC = IsIssueHasPersonInC(Convert.ToInt32(uu.id)),
                                    IssueCreatCompany = uu.comp,
                                    ProjectSurrKey = uu.projId
                               });
            //foreach(var r1 in dd)
            //{
            //         res.Add(new BuikIssueEntity{ IssueId = Convert.ToInt32(r1.IssueID),
            //                      IssuetypeId = r1.IssueTypeId,
            //                      IssueDetail = r1.IssueDetail,
            //                      IssueDateTimeCreate  = r1.IssueDateTimeCreate,
            //                      IssueCreatorName = GetUserFullNamebyId(r1.IssueCreator),//r2.user_fullname,
            //                      IssueCreatorPhoto = fpath + GetUserPhotobyId(r1.IssueCreator),
            //                      IssueProgress = Convert.ToInt16(r1.IssueProgress),
            //                      //IssueFloorPlanPhoto = AddFpath(r1.IssueFloorPlanPhoto),
            //                      IssueFloorPlanPhoto = String.IsNullOrEmpty(r1.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : r1.IssueFloorPlanPhoto,
            //                      IssueFile = r1.IssueFile,
            //                      IssueComment = GetIssueCommentCount(Convert.ToInt32(r1.IssueID)),
            //                      IssueLike = GetIssueLikeCount(Convert.ToInt32(r1.IssueID)),
            //                      IssueDeadline = r1.IssueDeadline,
            //                        IssueFileType = r1.IssueFileType,
            //         IsPersonINC = IsIssueHasPersonInC(Convert.ToInt32(r1.IssueID))}
            //                      );
            //         }
            if (dd2.Count() > 0)
            { return dd2.AsEnumerable().OrderByDescending(p => p.IssueId).ToList(); }
            return new List<BuikIssueEntity>();
        }

        public List<BuikIssueEntity> GetProjectIssueList22(Int32 prid)
        {
//            from p in context.ParentTable
//join c in context.ChildTable on p.ParentId equals c.ChildParentId into j1
//from j2 in j1.DefaultIfEmpty()
//group j2 by p.ParentId into grouped
//select new { ParentId = grouped.Key, Count = grouped.Count(t=>t.ChildId != null) }


        //    var qry =
        //from Cust in tblCust
        //from Order in tblOrder.Where(x => Cust.CustID == x.CustID)
        //                      .DefaultIfEmpty()
        //from Item in tblItem.Where(x => Order.OrderID == x.OrderID)
        //                    .DefaultIfEmpty()
        //group new { Cust, Order.OrderId, Item.ItemId } by new { Cust.CustID, Cust.CustName } into grp
        //let numItems = grp.Select(x => x.ItemId).Distinct().Count()
        //orderby numItems
        //select new
        //{
        //    ID = grp.Key.CustID,
        //    Name = grp.Key.CustName,
        //    numOrders = grp.Select(x => x.OrderId).Distinct().Count(),
        //    numItems,
        //};

             var db = new BuilkChameleonEntities();
             //var gg = db.Project_Issue.Where(p => p.proj_surr_key == prid).ToList();
             //var ggi = gg.ToList();
             //var rttt = (from pi1 in db.Project_Issue
             //            join us in db.T_USER on pi1.IssueCreator equals us.user_surr_key into j1
             //            from j2 in j1.DefaultIfEmpty()
             //            join pl in db.IssueComments on pi1.IssueID equals pl.IssueId into j3
             //            from j4 in j3.DefaultIfEmpty()
             //            join il in db.IssueLikes on pi1.IssueID equals il.IssueId into j5
             //            from j6 in j5.DefaultIfEmpty()
             //            join pic in db.IssuedPersonInChargeds on pi1.IssueID equals pic.IssueId into j7
             //            from j8 in j7.DefaultIfEmpty()
             //            group j8 by pi1.IssueID into grp
             //            select new
             //            {
             //                id = grp.Key,
             //                count = grp.Select(x => x.IssueId != null).Count()
             //            }
             //                ).ToList();

             //var tthh = rttt.ToList().Select(g => g.count);
            var qry = 
               ( from pi in db.Project_Issue.Where(p => p.proj_surr_key == prid)
                from us in db.T_USER.Where(x => pi.IssueCreator == x.user_surr_key)
                //.DefaultIfEmpty()
                from pl in db.IssueComments.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                from il in db.IssueLikes.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                from ppinc in db.IssuedPersonInChargeds.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                group new { pi,il,pl,ppinc,us} by new {pi.IssueID} into grp
                // let ppinc = grp.Select(x => x.ppinc.user_surr_key).Count(g => g != null)
                // let numcommet = grp.Select(x => x.pi).Count(g => g != null)
                //let nnn = grp.Select(x => x.pl).Distinct().Count()
                //let likecount = grp.Select(x => x.il).Distinct().Count(g => g != null)
                 
                orderby grp.Key.IssueID//numcommetn
                select new // BuikIssueEntity
                {                       
                    Issueid = grp.Key.IssueID,
                    IssueTypeId = grp.Select(x => x.pi.IssueTypeId).FirstOrDefault(),
                    IssueDetail = grp.Select(x => x.pi.IssueDetail).FirstOrDefault(),
                    IssueDateTimeCreate = grp.Select(x => x.pi.IssueDateTimeCreate).FirstOrDefault(),
                    IssueCreatorName = grp.Select(x => x.us.user_fullname).FirstOrDefault(),
                    IssueCreatorPhotho = grp.Select(x => x.us.user_avatar_filename).FirstOrDefault(),
                    IssueProgres = grp.Select(x => x.pi.IssueProgress).FirstOrDefault(),
                    IssueFloorPlanPhoto = grp.Select(x => x.pi.IssueFloorPlanPhoto).FirstOrDefault(),
                    IssueFile = grp.Select(x => x.pi.IssueFile).FirstOrDefault(),
                    IssueComment = grp.Select(x => x.pl).Distinct().Count(g => g != null),
                    IssueLike =grp.Select(x => x.il).Distinct().Count(g => g != null),
                    IssueDeadline = grp.Select(x => x.pi.IssueDeadline).FirstOrDefault(),
                    IssueFileType = grp.Select(x=> x.pi.IssueFileType).FirstOrDefault(),
                    IsPersonINC = grp.Select(x => x.ppinc).Distinct().Count(g => g != null),
                    ProjectSurrKey = grp.Select(x=> x.pi.proj_surr_key).FirstOrDefault(),
                    creatorid = grp.Select(x => x.pi.IssueCreator).FirstOrDefault(),
                    UserComp = grp.Select(x => x.us.user_company).FirstOrDefault()
                    //Creator = grp.Select(x => x.),
                   // IssueCreator = grp.Key.us.user_fullname,
                    //Detail = grp,
                    //hh = grp.Key.user_fullname,
                    //numorder = grp.Select(x=> x.us.user_fullname).FirstOrDefault(),
                  // numcommet = grp.Select(x => x.ppinc.user_surr_key).Count(g => g != null),
                   //ppinc = grp.Select(x => x.ppinc).Distinct().Count(g => g != null),
                   //numcommet = grp.Select(x => x.pl).Distinct().Count(g => g != null),
                   //likecount = grp.Select(x => x.il).Distinct().Count(g => g != null)
                   // numcommetn = grp.Select(x => x.pl.IssueId).Distinct().Count(),
                   // likecount,ppinc
                }).ToList().Select(uu => new BuikIssueEntity
                {
                    IssueId = uu.Issueid,
                    IssuetypeId = uu.IssueTypeId,//r1.IssueTypeId,
                    IssueDetail = uu.IssueDetail,//r1.IssueDetail,
                    IssueDateTimeCreate = uu.IssueDateTimeCreate,//r1.IssueDateTimeCreate,
                    IssueCreatorName = uu.IssueCreatorName,//GetUserFullNamebyId(r1.IssueCreator),//r2.user_fullname,
                    IssueCreatorPhoto = fpath + (String.IsNullOrEmpty(uu.IssueCreatorPhotho) ? "DefaultAva1.jpg" : uu.IssueCreatorPhotho),//fpath + GetUserPhotobyId(r1.IssueCreator),
                    IssueProgress = (int)uu.IssueProgres,//Convert.ToInt16(r1.IssueProgress),
                    //IssueFloorPlanPhoto = AddFpath(r1.IssueFloorPlanPhoto),
                    IssueFloorPlanPhoto = String.IsNullOrEmpty(uu.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : uu.IssueFloorPlanPhoto,
                    IssueFile = uu.IssueFile,//r1.IssueFile,
                    IssueComment = uu.IssueComment,//GetIssueCommentCount(Convert.ToInt32(uu.id)),
                    IssueLike = uu.IssueLike,////GetIssueLikeCount(Convert.ToInt32(uu.id)),
                    IssueDeadline = uu.IssueDeadline,//ideadline,
                    IssueFileType = uu.IssueFileType,//.filetype,
                    IsPersonINC = uu.IsPersonINC,//IsIssueHasPersonInC(Convert.ToInt32(uu.id)),
                    IssueCreatCompany = uu.UserComp,
                    IssueCreatorId = (Int32)uu.creatorid,
                    ProjectSurrKey = uu.ProjectSurrKey  
                });

          
          
           
            if (qry.Count() > 0)
            { return qry.AsEnumerable().OrderByDescending(p => p.IssueId).ToList(); }
            return new List<BuikIssueEntity>();
        }

        public List<BuikIssueEntity> GetProjectIssueList(Int32 prid)
        {
           
            var db = new BuilkChameleonEntities();
           
            var qry =
               (from pi in db.Project_Issue.Where(p => p.proj_surr_key == prid)
                from us in db.T_USER.Where(x => pi.IssueCreator == x.user_surr_key)
                //.DefaultIfEmpty()
                from pl in db.IssueComments.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                from il in db.IssueLikes.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                from ppinc in db.IssuedPersonInChargeds.Where(x => pi.IssueID == x.IssueId)
                .DefaultIfEmpty()
                from isuty in db.IssueTypes.Where(x => pi.IssueTypeId == x.IssueTypeId)
                .DefaultIfEmpty()
                group new { pi, il, pl, ppinc, us,isuty } by new { pi.IssueID } into grp
               
                //.OrderBy(p => p.ProductID)
//.Skip((page - 1) * PageSize)
//.Take(PageSize));

                orderby grp.Key.IssueID//numcommetn
                select new // BuikIssueEntity
                {
                    Issueid = grp.Key.IssueID,
                    IssueTypeId = grp.Select(x => x.pi.IssueTypeId).FirstOrDefault(),
                    IssueDetail = grp.Select(x => x.pi.IssueDetail).FirstOrDefault(),
                    IssueDateTimeCreate = grp.Select(x => x.pi.IssueDateTimeCreate).FirstOrDefault(),
                    IssueCreatorName = grp.Select(x => x.us.user_fullname).FirstOrDefault(),
                    IssueCreatorPhotho = grp.Select(x => x.us.user_avatar_filename).FirstOrDefault(),
                    IssueProgres = grp.Select(x => x.pi.IssueProgress).FirstOrDefault(),
                    IssueFloorPlanPhoto = grp.Select(x => x.pi.IssueFloorPlanPhoto).FirstOrDefault(),
                    IssueFile = grp.Select(x => x.pi.IssueFile).FirstOrDefault(),
                    IssueComment = grp.Select(x => x.pl).Distinct().Count(g => g != null),
                    IssueLike = grp.Select(x => x.il).Distinct().Count(g => g != null),
                    IssueDeadline = grp.Select(x => x.pi.IssueDeadline).FirstOrDefault(),
                    IssueFileType = grp.Select(x => x.pi.IssueFileType).FirstOrDefault(),
                    IsPersonINC = grp.Select(x => x.ppinc).Distinct().Count(g => g != null),
                    ProjectSurrKey = grp.Select(x => x.pi.proj_surr_key).FirstOrDefault(),
                    creatorid = grp.Select(x => x.pi.IssueCreator).FirstOrDefault(),
                    UserComp = grp.Select(x => x.us.user_company).FirstOrDefault(),
                    Issuetypname = grp.Select(x => x.isuty.IssueTypeName).FirstOrDefault()
                  
                }).ToList().Select(uu => new BuikIssueEntity
                {
                    IssueId = uu.Issueid,
                    IssuetypeId = uu.IssueTypeId,//r1.IssueTypeId,
                    IssueDetail = uu.IssueDetail,//r1.IssueDetail,
                    IssueDateTimeCreate = uu.IssueDateTimeCreate,//r1.IssueDateTimeCreate,
                    IssueCreatorName = uu.IssueCreatorName,//GetUserFullNamebyId(r1.IssueCreator),//r2.user_fullname,
                    IssueCreatorPhoto = fpath + (String.IsNullOrEmpty(uu.IssueCreatorPhotho) ? "DefaultAva1.jpg" : uu.IssueCreatorPhotho),//fpath + GetUserPhotobyId(r1.IssueCreator),
                    IssueProgress = (int)uu.IssueProgres,//Convert.ToInt16(r1.IssueProgress),
                    //IssueFloorPlanPhoto = AddFpath(r1.IssueFloorPlanPhoto),
                    IssueFloorPlanPhoto = String.IsNullOrEmpty(uu.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : uu.IssueFloorPlanPhoto,
                    IssueFile = uu.IssueFile,//r1.IssueFile,
                    IssueComment = uu.IssueComment,//GetIssueCommentCount(Convert.ToInt32(uu.id)),
                    IssueLike = uu.IssueLike,////GetIssueLikeCount(Convert.ToInt32(uu.id)),
                    IssueDeadline = uu.IssueDeadline,//ideadline,
                    IssueFileType = uu.IssueFileType,//.filetype,
                    IsPersonINC = uu.IsPersonINC,//IsIssueHasPersonInC(Convert.ToInt32(uu.id)),
                    IssueCreatCompany = uu.UserComp,
                    IssueCreatorId = (Int32)uu.creatorid,
                    ProjectSurrKey = uu.ProjectSurrKey,
                    IsTypeName = uu.Issuetypname
                });




            if (qry.Count() > 0)
            { return qry.AsEnumerable().OrderByDescending(p => p.IssueId).ToList(); }
            return new List<BuikIssueEntity>();
        }

        //private int GetMaxPageSize(Int32 prid)
        //{ 
            //get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
            
        //}

        public List<BuikIssueEntity> GetProjectIssueList(Int32 prid,int PageSize,int page)
        {
           
            var db = new BuilkChameleonEntities();
            var maxpage = (int)Math.Ceiling((decimal)db.Project_Issue.Where(p => p.proj_surr_key == prid).Count()/PageSize);
            if (page <= maxpage)
            {
                var qry =
                   (from pi in db.Project_Issue.Where(p => p.proj_surr_key == prid).OrderBy(p => p.IssueID).Skip((page - 1) * PageSize).Take(PageSize)
                    from us in db.T_USER.Where(x => pi.IssueCreator == x.user_surr_key)
                    //.DefaultIfEmpty()
                    from pl in db.IssueComments.Where(x => pi.IssueID == x.IssueId)
                    .DefaultIfEmpty()
                    from il in db.IssueLikes.Where(x => pi.IssueID == x.IssueId)
                    .DefaultIfEmpty()
                    from ppinc in db.IssuedPersonInChargeds.Where(x => pi.IssueID == x.IssueId)
                    .DefaultIfEmpty()
                    from isuty in db.IssueTypes.Where(x => pi.IssueTypeId == x.IssueTypeId)
                    .DefaultIfEmpty()
                    group new { pi, il, pl, ppinc, us, isuty } by new { pi.IssueID } into grp

                    //.OrderBy(p => p.ProductID)


                   // orderby grp.Key.IssueID//numcommetn
                    select new // BuikIssueEntity
                    {
                        Issueid = grp.Key.IssueID,
                        IssueTypeId = grp.Select(x => x.pi.IssueTypeId).FirstOrDefault(),
                        IssueDetail = grp.Select(x => x.pi.IssueDetail).FirstOrDefault(),
                        IssueDateTimeCreate = grp.Select(x => x.pi.IssueDateTimeCreate).FirstOrDefault(),
                        IssueCreatorName = grp.Select(x => x.us.user_fullname).FirstOrDefault(),
                        IssueCreatorPhotho = grp.Select(x => x.us.user_avatar_filename).FirstOrDefault(),
                        IssueProgres = grp.Select(x => x.pi.IssueProgress).FirstOrDefault(),
                        IssueFloorPlanPhoto = grp.Select(x => x.pi.IssueFloorPlanPhoto).FirstOrDefault(),
                        IssueFile = grp.Select(x => x.pi.IssueFile).FirstOrDefault(),
                        IssueComment = grp.Select(x => x.pl).Distinct().Count(g => g != null),
                        IssueLike = grp.Select(x => x.il).Distinct().Count(g => g != null),
                        IssueDeadline = grp.Select(x => x.pi.IssueDeadline).FirstOrDefault(),
                        IssueFileType = grp.Select(x => x.pi.IssueFileType).FirstOrDefault(),
                        IsPersonINC = grp.Select(x => x.ppinc).Distinct().Count(g => g != null),
                        ProjectSurrKey = grp.Select(x => x.pi.proj_surr_key).FirstOrDefault(),
                        creatorid = grp.Select(x => x.pi.IssueCreator).FirstOrDefault(),
                        UserComp = grp.Select(x => x.us.user_company).FirstOrDefault(),
                        Issuetypname = grp.Select(x => x.isuty.IssueTypeName).FirstOrDefault()

                    })
                    //.Skip((page - 1) * PageSize)
                    //.Take(PageSize))
                        .ToList().Select(uu => new BuikIssueEntity
                    {
                        IssueId = uu.Issueid,
                        IssuetypeId = uu.IssueTypeId,//r1.IssueTypeId,
                        IssueDetail = uu.IssueDetail,//r1.IssueDetail,
                        IssueDateTimeCreate = uu.IssueDateTimeCreate,//r1.IssueDateTimeCreate,
                        IssueCreatorName = uu.IssueCreatorName,//GetUserFullNamebyId(r1.IssueCreator),//r2.user_fullname,
                        IssueCreatorPhoto = fpath + (String.IsNullOrEmpty(uu.IssueCreatorPhotho) ? "DefaultAva1.jpg" : uu.IssueCreatorPhotho),//fpath + GetUserPhotobyId(r1.IssueCreator),
                        IssueProgress = (int)uu.IssueProgres,//Convert.ToInt16(r1.IssueProgress),
                        //IssueFloorPlanPhoto = AddFpath(r1.IssueFloorPlanPhoto),
                        IssueFloorPlanPhoto = String.IsNullOrEmpty(uu.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : uu.IssueFloorPlanPhoto,
                        IssueFile = uu.IssueFile,//r1.IssueFile,
                        IssueComment = uu.IssueComment,//GetIssueCommentCount(Convert.ToInt32(uu.id)),
                        IssueLike = uu.IssueLike,////GetIssueLikeCount(Convert.ToInt32(uu.id)),
                        IssueDeadline = uu.IssueDeadline,//ideadline,
                        IssueFileType = uu.IssueFileType,//.filetype,
                        IsPersonINC = uu.IsPersonINC,//IsIssueHasPersonInC(Convert.ToInt32(uu.id)),
                        IssueCreatCompany = uu.UserComp,
                        IssueCreatorId = (Int32)uu.creatorid,
                        ProjectSurrKey = uu.ProjectSurrKey,
                        IsTypeName = uu.Issuetypname
                    });
                if (qry.Count() > 0)
                { return qry.AsEnumerable().OrderByDescending(p => p.IssueId).ToList(); }
            }


           
            return new List<BuikIssueEntity>();
        }


        private string AddFpath(string inp)
        {
            if (inp.Length < 0) { return this.fpath + "DefaultAva1.jpg"; }
            else return string.Concat(fpath, inp);
        }
        public List<BuikIssueEntity> GetProjectIssueListByIssueId(string issueid)
        {
            List<BuikIssueEntity> res = new List<BuikIssueEntity>();
            Int32 rrr = Convert.ToInt32(issueid);
            var dd = new BuilkChameleonEntities().Project_Issue.Where(p => p.IssueID == rrr);          
            foreach (var r1 in dd)
            {
                res.Add(new BuikIssueEntity
                {
                    IssueId = Convert.ToInt32(r1.IssueID),
                    IssuetypeId = r1.IssueTypeId,
                    IssueDetail = r1.IssueDetail,
                    IssueDateTimeCreate = r1.IssueDateTimeCreate,
                    IssueCreatorName =GetUserFullNamebyId((Int32)r1.IssueCreator),
                    IssueCreatorPhoto = fpath + GetUserPhotobyId((Int32)r1.IssueCreator),
                    IssueProgress = Convert.ToInt16(r1.IssueProgress),
                    IssueFloorPlanPhoto = r1.IssueFloorPlanPhoto,
                    IssueFile = r1.IssueFile,
                    IssueComment = GetIssueCommentCount(Convert.ToInt32(r1.IssueID)),
                    IssueLike = GetIssueLikeCount(Convert.ToInt32(r1.IssueID)),
                    IssueDeadline = r1.IssueDeadline,
                    IssueFileType = r1.IssueFileType,
                    IsPersonINC = IsIssueHasPersonInC(Convert.ToInt32(r1.IssueID)),
                    IssueCreatorId = (Int32)r1.IssueCreator
                }
                             );
            }
            if (res.Count() > 0)
            { return res.AsEnumerable().OrderByDescending(p => p.IssueId).ToList(); }
            return new List<BuikIssueEntity>();
        }
        private int IsIssueHasPersonInC(Int32 issueid)
        {
            int res = 0;
            try { res = new BuilkChameleonEntities().IssuedPersonInChargeds.Where(p => p.IssueId == issueid).Count(); }
            catch {}
            if (res > 1) { return 1; }
            return res;
        }
        private string GetUserFullNamebyId(Int32 uid)
        { return new BuilkChameleonEntities().T_USER.Where(p => p.user_surr_key == uid).Select(q => q.user_fullname).SingleOrDefault(); }
        private string GetUserPhotobyId(Int32 uid)
        {
            string res="";
            try
            {
                res = new BuilkChameleonEntities().T_USER.Where(p => p.user_surr_key == uid).Select(q => q.user_avatar_filename).SingleOrDefault();
            }
            catch { res = "DefaultAva1.jpg"; }
            return res;
        }
        private string GetUNamebyId(Int32 uid)
        {
            return new BuilkChameleonEntities().T_USER.Where(p => p.user_surr_key == uid).Select(p => p.user_fullname).FirstOrDefault();
        }
        private string GetUPhotobyId(Int32 uid)
        {
            return new BuilkChameleonEntities().T_USER.Where(p => p.user_surr_key == uid).Select(p => p.user_avatar_filename).FirstOrDefault();
        }
        public List<BuikIssueComment> GetIssueCommentDetail(Int32 issueids)
        {
            List<BuikIssueComment> res = new List<BuikIssueComment>();
            var db = from r1 in new BuilkChameleonEntities().IssueComments                     
                     where r1.IssueId == issueids
                     orderby r1.IssueCommentId
                     select new BuikIssueComment { 
                        CDatetime = r1.IssueCommentDateTime,
                        CommentD = r1.IssueCommentDetail,
                        CUid = (Int32)r1.IssueCommentUserId,
                        CommentIssueId = r1.IssueCommentId
                     };
            if(db.Count() > 0)
            {
                foreach (var r in db)
                {
                    r.CUserName = GetUNamebyId(r.CUid);
                    r.CUPhoto = GetUPhotobyId(r.CUid);
                }
                return db.ToList();
            }
             return res;
                        
        }
        public List<BuikIssueComment> GetIssueCommentDetail2(Int32 issueids)
        { 
            List<BuikIssueComment> res = new List<BuikIssueComment>();
            var db = new BuilkChameleonEntities();
            var ress = from r1 in db.T_USER
                       join r2 in db.IssueComments on r1.user_surr_key equals r2.IssueCommentUserId
                     where r2.IssueId == issueids
                     orderby r2.IssueCommentId
                     select new BuikIssueComment
                     {
                         CDatetime = r2.IssueCommentDateTime,
                         CommentD = r2.IssueCommentDetail,
                         CUid = (Int32)r2.IssueCommentUserId,
                         CUserName = r1.user_fullname,
                         CUPhoto = string.Concat(fpath, String.IsNullOrEmpty(r1.user_avatar_filename) ? "DefaultAva1.jpg" : r1.user_avatar_filename),
                         CommentIssueId = r2.IssueCommentId
                     };
           
            return ress.ToList();

        }
        public int GetUserIdByName(string username)
        {
            string[] words = username.Split(' ');
            string fname = words[0];string  sname = words[1];
            var rr = new BuilkChameleonEntities().T_USER.Where(p => p.user_name.Equals(fname) && p.user_surname.Equals(sname)).FirstOrDefault();

            int res = rr.user_surr_key;
            return res;
        }
        public int GetUserIdByEmail(string usermail)
        {
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    var s = (from u in db.T_USER
                             where u.user_email.Equals(usermail)
                             select u.user_surr_key
                                 ).FirstOrDefault();
                    return s;
                }
            }
            catch { return 0; }
           
        }
        public int GetMaxProjSurrogateKey()
        {
            int res = new BuilkChameleonEntities().T_PROJECT.Max(p => p.proj_surr_key);
            return res;
        }
        public Int32 GetMaxProjectFloorPlanKey()
        {
            Int32 res;
            try { res = new BuilkChameleonEntities().Project_FloorPlan.Max(p => p.project_floorplan_id); }
            catch { return 1; }
            return res+1 ;
        }
        public List<BuikProjectEntity> GetProjectMobileEntityByUser(int id)
        {
            List<BuikProjectEntity> res = new List<BuikProjectEntity>();
            using (var db = new BuilkChameleonEntities())
            {
                var query = from b in db.T_PROJECT
                            where b.create_user_id == id
                            orderby b.create_date descending
                            select b;
                foreach (var bb in query)
                {
                    res.Add(new BuikProjectEntity(bb.proj_surr_key,bb.create_user_id, bb.proj_code, bb.proj_name, bb.proj_owner, bb.projtype_id, bb.proj_location, bb.proj_start_date.ToString(), bb.proj_finish_date.ToString(),bb.proj_photo_filename));
                }
            }
            return res;
        }
        public string AddNewProjectMobile(BuikProjectEntity pr)
        {
            string res ="";
            var db = new BuilkChameleonEntities();
            T_PROJECT p = new T_PROJECT();
            
            p.proj_code = "j-1002";// pr.ProjectCode;
            p.proj_name = pr.ProjectName;                     
            p.proj_owner = GetUserFullNamebyId(pr.createuserid);
            p.projtype_id = pr.ProjectType;
            p.proj_province = pr.ProjectProvince;
            p.proj_country = pr.ProjectCountry;
           
            p.proj_start_date = Convert.ToDateTime(pr.ProjectStartDate);
            p.proj_finish_date = Convert.ToDateTime(pr.ProjectEndDate);
            p.proj_photo_filename = pr.ProjectTimelinePhoto;
            p.proj_owner = pr.CustomerName;
          
            p.create_user_id = pr.createuserid;
            p.proj_owner_id = 85615;

            string a = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            try{
            string sql1 = "Insert into T_PROJECT(proj_code,proj_name,proj_province,proj_country,create_user_id,create_date,proj_owner_id,projtype_id,proj_start_date,proj_finish_date,proj_owner,projstatus_id)" ;
           
                sql1 += "values('"+p.proj_code+"','"+p.proj_name+"','"+p.proj_province+"','" +p.proj_country+ "',"+p.create_user_id+",'"+a+"',"+p.proj_owner_id+",'"+p.projtype_id+"','"+((DateTime)p.proj_start_date).ToString("d")+"','"+((DateTime)p.proj_finish_date).ToString("d")+"','"+pr.CustomerName+"',1)";
            db.Database.ExecuteSqlCommand(sql1);
            int prid = GetMaxProjSurrogateKey();
           
            this.AddProjectCollaboration(prid, p.create_user_id,2,0);
             res = prid.ToString();
                    ;
            }
            catch{return "0";}
            return res;
        }

        public string AddNewProjectMobileWithLat(BuikProjectEntity pr)
        {
            string res = "";
            var db = new BuilkChameleonEntities();
            T_PROJECT p = new T_PROJECT();

            p.proj_code = "j-1002";// pr.ProjectCode;
            p.proj_name = pr.ProjectName;
           // p.proj_owner = GetUserFullNamebyId(pr.createuserid);
            p.projtype_id = pr.ProjectType;
            p.proj_province = pr.ProjectProvince;
            p.proj_country = pr.ProjectCountry;

            p.proj_start_date = Convert.ToDateTime(pr.ProjectStartDate);
            p.proj_finish_date = Convert.ToDateTime(pr.ProjectEndDate);
            p.proj_photo_filename = pr.ProjectTimelinePhoto;
           // p.proj_owner = pr.CustomerName;

            p.create_user_id = pr.createuserid;
           // p.proj_owner_id = 85615;
           // p.proj_owner_id = pr.createuserid;
            p.proj_latitude = pr.latitude;
            p.proj_longitude = pr.longtitude;
            p.proj_owner_id = 85615;
            string a = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            try
            {
                string sql1 = "Insert into T_PROJECT(proj_code,proj_name,proj_province,proj_country,create_user_id,create_date,proj_owner_id,projtype_id,proj_start_date,proj_finish_date,proj_owner,projstatus_id,proj_latitude,proj_longitude)";

                sql1 += "values('" + p.proj_code + "','" + p.proj_name + "','" + p.proj_province + "','" + p.proj_country + "'," + p.create_user_id + ",'" + a + "'," + p.proj_owner_id + ",'" + p.projtype_id + "','" + ((DateTime)p.proj_start_date).ToString("G", CultureInfo.CreateSpecificCulture("en-US")) + "','" + ((DateTime)p.proj_finish_date).ToString("G", CultureInfo.CreateSpecificCulture("en-US")) + "','" + pr.CustomerName + "',1," + pr.latitude + "," + pr.longtitude + ")";


              //  string sql1 = "Insert into T_PROJECT(proj_code,proj_name,proj_province,proj_country,create_user_id,create_date,proj_owner_id,projtype_id,proj_owner,projstatus_id,proj_latitude,proj_longitude)";

                //sql1 += "values('" + p.proj_code + "','" + p.proj_name + "','" + p.proj_province + "','" + p.proj_country + "'," + p.create_user_id + ",'" + a + "'," + p.proj_owner_id + ",'" + p.projtype_id + "','" + pr.CustomerName + "',1," + pr.latitude + "," + pr.longtitude + ")";
               
                
                db.Database.ExecuteSqlCommand(sql1);
                int prid = GetMaxProjSurrogateKey();

                this.AddProjectCollaboration(prid, p.create_user_id, 2, 0);
                res = prid.ToString();
                ;
            }
            catch { return "0"; }
            return res;
        }

        public string UpdateProjectProfilePhoto(Int32 prid, string filelocation)
        {
           string sql1 = "Update T_PROJECT SET proj_photo_filename='"+filelocation+"' WHERE proj_surr_key="+prid+"";
            var res = new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
            return res.ToString();
        }

        public string AddProjectFloorPlanPhoto(Int32 prid, string filelocation,string floorplanname)
        {
            string sql1 = "Insert into Project_FloorPlan (project_floorplan_id,proj_surr_key,floorplan_photo,floorplan_name) ";
            sql1 += "values ("+GetMaxProjectFloorPlanKey()+","+prid+",'"+filelocation+"','"+ floorplanname +"')";
            var res = new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
            return res.ToString();
        }

        public void AddProjectFloorPlan(string prid, string floorplanfilename)
        {
            using (var db = new BuilkChameleonEntities())
            {
                Project_FloorPlan p = new Project_FloorPlan();
                p.floorplan_photo = floorplanfilename;
                p.proj_surr_key = Convert.ToInt32(prid);
                db.Project_FloorPlan.Add(p);
                db.SaveChanges();
            }
        }
        public void EditProjectMobile(BuikProjectEntity pr1)
        {
            var db = new BuilkChameleonEntities();
            var pr = db.T_PROJECT.Where(p => p.proj_surr_key == pr1.prid).SingleOrDefault();
          
        }
        public void AddProjectCollaboration(string prid, string userid)
        {
            AddProjectCollaboration(Convert.ToInt32(prid), Convert.ToInt32(userid),2);
        }
        public void AddProjectCollaboration(Int32 prid, Int32 userid,int sta)
        {
            //int jj = 0;
            using (var db = new BuilkChameleonEntities())
            {
                int j = db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(userid) && p.user_surr_key.Equals(userid)).Count();
                if (j == 0)//db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(inviter) && p.user_surr_key.Equals(userid)).Count() == 0)
                {
                    Project_Collaboration pcol = new Project_Collaboration();
                    pcol.proj_surr_key = prid;
                    pcol.user_surr_key = userid;
                    pcol.inviter_surr_key = userid;
                    pcol.isFav = 0;
                    pcol.invitation_status = sta;
                    string sql1 = "Insert into Project_Collaboration(proj_surr_key,user_surr_key,inviter_surr_key,isFav,invitation_status,IsShow)";
                    sql1 += "values(" + pcol.proj_surr_key + "," + pcol.user_surr_key + "," + pcol.inviter_surr_key + "," + pcol.isFav + "," + pcol.invitation_status + ",1)";
                    db.Database.ExecuteSqlCommand(sql1);
                }
            } ;

        }
        public int AddProjectCollaboration(Int32 prid, Int32 userid, int sta, int isfav,Int32 inviter)
        {
            int jj = 0;
            using (var db = new BuilkChameleonEntities())
            {
                int j = db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(inviter) && p.user_surr_key.Equals(userid)).Count();
                if (j ==0)//db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(inviter) && p.user_surr_key.Equals(userid)).Count() == 0)
                {

                    Project_Collaboration pcol = new Project_Collaboration();
                    pcol.proj_collaboration_id = (int)db.Project_Collaboration.Max(p => p.proj_collaboration_id) + 1;
                    pcol.proj_surr_key = prid;
                    pcol.user_surr_key = userid;
                    pcol.inviter_surr_key = userid;
                    pcol.isFav = isfav;
                    pcol.invitation_status = sta;

                    string sql1 = "Insert into Project_Collaboration(proj_surr_key,user_surr_key,inviter_surr_key,isFav,invitation_status,IsShow)";
                    sql1 += "values(" + pcol.proj_surr_key + "," + pcol.user_surr_key + "," + pcol.inviter_surr_key + "," + pcol.isFav + "," + pcol.invitation_status + ",1)";
                    db.Database.ExecuteSqlCommand(sql1);
                    jj = 1;
                }

            } return jj;
        }

        public void AddProjectCollaboration(Int32 prid, Int32 userid, int sta,int isfav)
        {
            using (var db = new BuilkChameleonEntities())
            {
               

                    Project_Collaboration pcol = new Project_Collaboration();
                    pcol.proj_collaboration_id = (int)db.Project_Collaboration.Max(p => p.proj_collaboration_id) + 1;
                    pcol.proj_surr_key = prid;
                    pcol.user_surr_key = userid;
                    pcol.inviter_surr_key = userid;
                    pcol.isFav = isfav;
                    pcol.invitation_status = sta;

                    string sql1 = "Insert into Project_Collaboration(proj_surr_key,user_surr_key,inviter_surr_key,isFav,invitation_status,IsShow)";
                    sql1 += "values(" + pcol.proj_surr_key + "," + pcol.user_surr_key + "," + pcol.inviter_surr_key + "," + pcol.isFav + "," + pcol.invitation_status + ",1)";
                    db.Database.ExecuteSqlCommand(sql1);
                
            }
        }

        public string AddProjectCollaborationWithinvitedId(Int32 prid, Int32 userid,Int32 inviter, int sta)
        {
            using (var db = new BuilkChameleonEntities())
            {
                if (db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(inviter) && p.user_surr_key.Equals(userid)).Count() > 0)
                {
                    return "Duplicate Invitation";
                }
                else
                {
                    Project_Collaboration pcol = new Project_Collaboration();
                   // pcol.proj_collaboration_id = (int)db.Project_Collaboration.Max(p => p.proj_collaboration_id) + 1;
                    pcol.proj_surr_key = prid;
                    pcol.user_surr_key = userid;
                    pcol.inviter_surr_key = inviter;
                    pcol.isFav = 0;
                    pcol.invitation_status = sta;
                    string sql1 = "Insert into Project_Collaboration(proj_surr_key,user_surr_key,inviter_surr_key,isFav,invitation_status,IsShow)";
                    sql1 += "values(" + pcol.proj_surr_key + "," + pcol.user_surr_key + "," + pcol.inviter_surr_key + "," + pcol.isFav + "," + pcol.invitation_status + ",1)";
                    return db.Database.ExecuteSqlCommand(sql1).ToString();
                }
            }
        }
        private bool CheckDupInvitation(Int32 prid, Int32 inviter, Int32 invited)
        { 
            var db = new BuilkChameleonEntities();
            if(db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(prid) && p.inviter_surr_key.Equals(inviter) && p.user_surr_key.Equals(invited)).Count() > 0)
            {
                return false;
            }else return true;
        }
        public string AddProjectCollaboration(BuikProjectCollaborationEntity pc)
        {
            using (var db = new BuilkChameleonEntities())
            {
                if (db.Project_Collaboration.Where(p => p.proj_surr_key.Equals(pc.ProjectId) && p.inviter_surr_key.Equals(pc.InviterId) && p.user_surr_key.Equals(pc.InvitedUserid)).Count() > 0)
                {
                    return "Duplicate Invitation";
                }
                else{
                Project_Collaboration pcol = new Project_Collaboration();
               pcol.proj_surr_key = pc.ProjectId;
               pcol.user_surr_key = pc.InvitedUserid;
               pcol.inviter_surr_key = pc.InviterId;
               pcol.isFav = 0;
               pcol.invitation_status = 1;
              string sql1 = "Insert into Project_Collaboration(proj_surr_key,user_surr_key,inviter_surr_key,isFav,invitation_status,IsShow)";
                sql1 += "values(" + pcol.proj_surr_key + "," + pcol.user_surr_key + "," + pcol.inviter_surr_key + "," + pcol.isFav + "," + pcol.invitation_status + ",1)";
                return db.Database.ExecuteSqlCommand(sql1).ToString();
                
                }
            }
        }

        public List<IntStringObj> GetProjectFloorPlan(Int32 prid)
        {
            var res = (from t in new BuilkChameleonEntities().Project_FloorPlan
                       where t.proj_surr_key == prid
                       select new IntStringObj
                       {
                           number = t.project_floorplan_id,
                           msg = t.floorplan_photo,
                           det = t.floorplan_name
                       }).ToList();
            return res;
        }
        public List<BuikProjectCollaborationEntity> GetProjectCollaborationRequest(Int32 userid)
        {
            List<BuikProjectCollaborationEntity> res = new List<BuikProjectCollaborationEntity>();
                var rr = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == userid && p.invitation_status == 1).ToList();
                foreach (var r in rr)
                {
                    res.Add(new BuikProjectCollaborationEntity
                    {
                        ProjectId = r.proj_collaboration_id,
                        InvitedUserid = r.user_surr_key,
                        InviterId = r.inviter_surr_key,
                        ProjectCollaborationId = r.proj_collaboration_id,
                        InviteStatus = r.invitation_status
                    });
                }
                return res;
        }
        public int UpdateProjectCollaboration(string prid, string userid, string inviter, int stats)
        {
            int h = 0;
            using (var db = new BuilkChameleonEntities())
            {
                string sql1 = "update Project_Collaboration set invitation_status = " + stats + " where proj_surr_key = " + Convert.ToInt32(prid) + " and user_surr_key = " + Convert.ToInt32(inviter) + "";
                h = db.Database.ExecuteSqlCommand(sql1);

            } return h;
            
        }

        public int UpdateProjectCollaboration2(string prid, string userid, string inviter, int stats)
        {
            int h = 0;
            using (var db = new BuilkChameleonEntities())
            {
                string sql1 = "update Project_Collaboration set invitation_status = " + stats + " where proj_surr_key = " + Convert.ToInt32(prid) + " and user_surr_key = " + Convert.ToInt32(userid) + "";
                h = db.Database.ExecuteSqlCommand(sql1);

            } return h;

        }

        public int UpdateProjectCompany(Int32 prid, Int32 cid)
        {
            int h = 0;
            using (var db = new BuilkChameleonEntities())
            {
               string sql1 = "update T_Project set  company_surr_key = "+ cid +" where proj_surr_key = " + prid + "";
                h = db.Database.ExecuteSqlCommand(sql1);

            } return h;
        }

        public List<ProjColList> GetProjectCollabarationList(int prid)
        {
            var db = new BuilkChameleonEntities();
            var res = (from p in db.T_USER
                       join q in db.Project_Collaboration on p.user_surr_key equals q.user_surr_key
                       where q.proj_surr_key == prid

                       select new ProjColList
                       {
                           username = p.user_fullname,
                           userid = q.user_surr_key,
                           invitestatus = q.invitation_status == 2 ? "Accepted" : "Pending",
                           memberstatus = q.inviter_surr_key == q.user_surr_key ? "Creator" : "Member"
                       });

            // var rr = res.Select(p => p.username).Distinct();
            return res.Distinct().ToList();
        }

        public List<ProjColList> GetProjectDetailMobileByEmail(Int32 prid)
        {
            var db = new BuilkChameleonEntities();
            var res = (from p in db.T_USER
                      join q in db.Project_Collaboration on p.user_surr_key equals q.user_surr_key
                      where q.proj_surr_key == prid
                      
                      select new ProjColList{
                          username = p.user_fullname,
                          userid = q.user_surr_key,
                          invitestatus = q.invitation_status == 2 ? "Accepted" : "Pending",
                          memberstatus = q.inviter_surr_key == q.user_surr_key ? "Creator" : "Member"
                      });

           // var rr = res.Select(p => p.username).Distinct();
            return res.Distinct().ToList();
        }

        public List<ProjColList> GetProjectEmailInvitedCollaboration(Int32 prid, Int32 userid)
        {
            var db = new BuilkChameleonEntities();
            var res = db.T_INVITES.Where(p => p.invitation_status=="0" &&p.project_surr_key == prid && p.user_surr_key == userid);
            var res2 = from r in res
                       select new ProjColList
                       {
                           username = r.email_invited,
                           userid = 0,
                           invitestatus = "Pending"
                       };
                //(from p in db.T_INVITES
                      // where p.project_surr_key == prid && p.user_surr_key == userid //&& p.invitation_status=="0"
                      // select new ProjColList
                      // {
                      //     username = p.email_invited,
                      //     userid = 0,
                      //     invitestatus = "Pending"
                      // });
            return res2.Distinct().ToList();
                       
        }


        public int RemoveProjectCollaboration(Int32 prid, Int32 userid, Int32 inviter)
        {
           
            string sql1 = "delete from Project_Collaboration where proj_surr_key = " + prid + " and inviter_surr_key = " + inviter + "";
            return new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
        }

        public int RemoveProjectCollaboration(Int32 prid, Int32 userid)
        {
           
            string sql1 = "delete from Project_Collaboration where proj_surr_key = " + prid + " and user_surr_key = " + userid + "";
            return new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
      
        }

        public int RemoveEmailInvitation(string email,Int32 prid,Int32 userid)
        {
            string sql1 = "delete from T_INVITES where proj_surr_key = " + prid + " and user_surr_key = " + userid + " and email_invited = '"+ email +"'";
            return new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
        }

        public Int32 GetProjectCreator(Int32 prid)
        {
            var res = new BuilkChameleonEntities().T_PROJECT.Where(p => p.proj_surr_key == prid).Select(o => o.create_user_id).FirstOrDefault();
            return res;
        }

        public int RemoveIssueComment(Int32 icid)
        {
           
                string sql1 = "delete from IssueComment where IssueCommentId = " + icid + "";
                return new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
               
        }

        public Int32 GetProjColInviter(Int32 prid,Int32 inviteduser)
        {
            return new BuilkChameleonEntities().Project_Collaboration.Where(p => p.proj_surr_key == prid && p.user_surr_key == inviteduser).Select(q => q.inviter_surr_key).SingleOrDefault();
        }

        public int RemoveLike(Int32 issueid, Int32 userid)
        {
            string sql1 = "delete from IssueLike where IssueId = " + issueid + " and LikeUserId = " + userid + "";
            return new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
            
        }

        private string GetProjectNamebyId(Int32 id)
        {
            return new BuilkChameleonEntities().T_PROJECT.Where(p => p.proj_surr_key == id).Select(q => q.proj_name).SingleOrDefault();
        }

        public List<IntStringObj> GetProjectListForUser(Int32 userid)
        {
            List<IntStringObj> res = new List<IntStringObj>();
            var r1 = new BuilkChameleonEntities().Project_Collaboration.Where(p => p.user_surr_key == userid).Select(q => q.proj_surr_key);
            foreach(int r2 in r1)
            {
                IntStringObj g = new IntStringObj();
                g.number = r2;
                g.msg = GetProjectNamebyId(r2);
                res.Add(g);
            }
            return res;
        }

        public int AddNotification(string fuid,string tuid,int isR,string msg,string nf, string did)
        {
            string sql1 = "insert into T_Notification (fromUserID,toUserID,isRead,notificationMsg,notificationType,destinationID,timeStamp) values ('" + fuid + "','" + tuid + "'," + isR + ",'" + msg + "','" + nf + "','" + did + "','" + DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US")) + "')"; 
            int res = 0;
            using (var db = new BuilkChameleonEntities())
            {
                res = db.Database.ExecuteSqlCommand(sql1);
            }
            return res;
        }

        public int UpdateNotifIsRead(Int32 ID, int val)
        {
            string sql1 = "update T_Notification set isRead = "+ val +" where ID = "+ID+"";
            int res = 0;
            using (var db = new BuilkChameleonEntities())
            {
                res = db.Database.ExecuteSqlCommand(sql1);
            }
            return res;
        }

        public List<NotificationEntity> GetNotificationById(string uid)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var res = from r in db.T_Notification
                          orderby r.ID descending
                         where r.toUserID.Equals(uid)
                         
                         select new NotificationEntity
                         {
                             Id = r.ID,
                             fromUID = r.fromUserID,
                             toUID = r.toUserID,
                             IsR = r.isRead,
                             Msg = r.notificationMsg,
                             NotType = r.notificationType,
                             DestId = r.destinationID,
                             Timestamp = r.timeStamp
                         };
                return res.ToList();
            }
           
            
        }

        public int RemoveFPPhoto(string filepath)
        {
            int i = 0;
            using (var db = new BuilkChameleonEntities())
            {
                string sql1 = "delete from Project_FloorPlan where floorplan_photo = '" + filepath + "'";
                i= db.Database.ExecuteSqlCommand(sql1);
            }
            return i;
        }

       

        public string GetCompanyByProjectId(Int32 prid)
        {
            var db = new BuilkChameleonEntities();
            try
            {
                var ss = from p in db.T_PROJECT
                         where p.proj_surr_key == prid
                         join q in db.T_COMPANY on p.company_surr_key equals q.company_surr_key
                         select q.company_name;
                return ss.FirstOrDefault();
            }
            catch { return "SiteWalk"; }
        }

        public List<IntStringObj> GetCompanyNamebyId(int uid)
        {
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    var res = (from r in db.T_EMPLOYEE
                              where r.user_surr_key == uid
                              join s in db.T_COMPANY on r.company_surr_key equals s.company_surr_key
                              select
                              new
                              {
                                  msg1 = s.company_name,//? null:s.company_name,
                                  number1 = r.company_surr_key
                              }).ToList().Select(gg => new IntStringObj
                              {
                                  msg = gg.msg1,
                                  number = gg.number1
                              });
                    return res.ToList();
                }
            }
            catch { return new List<IntStringObj>(); }
        }

        public int EditProjectName(int prid, string name)
        {
            int res = 0;
            try {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set proj_name = '" + name + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { return 0; }
            return res;
        }

        public int EditProjCustomer(int prid, string cname)
        {
            int res = 0;
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set proj_owner = '" + cname + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { return 0; }
            return res;
        }

        public int EditProjectCountry(int prid, string c)
        {
            int res = 0;
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set  proj_country = '" + c + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { }
            return res;
        }

        public int EditProjectProvince(int prid, string pr)
        {
            int res = 0;
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set  proj_province = '" + pr + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { }
            return res;
        }

        public int EditProjectStartDate(int prid, string d)
        {
            int res = 0;
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set  proj_start_date = '" + d + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { }
            return res;
        }

        public int EditProjectEndDate(int prid, string d)
        {
            int res = 0;
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "update T_Project set  proj_finish_date = '" + d + "' where proj_surr_key = " + prid + "";
                    res = db.Database.ExecuteSqlCommand(sql1);
                }

            }
            catch { }
            return res;
        }

        public string GetProjectComapanyName(int prid)
        {
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string g = (from r in db.T_PROJECT
                                where r.proj_surr_key == prid
                                join s in db.T_COMPANY on r.company_surr_key equals s.company_surr_key
                                select s.company_name).FirstOrDefault();
                    return g;
                }
            }
            catch { return "SiteWalk"; }
        }

        public string GetVersion()
        {
            
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    var s = db.Version_Control.Where(p => p.VersionID == db.Version_Control.Max(q => q.VersionID)).FirstOrDefault();
                    return s.VersionNum;// +" d " + s.DateRelease;
                }
            }
            catch { return ""; }
        }

        private int GetMaxVersion()
        {
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    int s = db.Version_Control.Where(p => p.VersionID == db.Version_Control.Max(q => q.VersionID)).Select(r => r.VersionID).FirstOrDefault();//.Where(p => p.VersionID == db.Version_Control.Max(q => q.VersionID)).FirstOrDefault();
                    return s+1;
                }
            }
            catch { return 0; }
        }

        public int SetVersion(string f)
        {
            try
            {
                using (var db = new BuilkChameleonEntities())
                {
                    string sql1 = "insert into Version_Control(VersionID,VersionNum,DateRelease)";
                    sql1 += "values ("+GetMaxVersion()+",'"+f+"','"+DateTime.Now.ToShortDateString()+"')";
                    return db.Database.ExecuteSqlCommand(sql1);
                }
            }
            catch { return 0; ; }
        }

        public string GetLatestIssuePhoto(Int32 prid)
        {
            var db = new BuilkChameleonEntities();
            var res = (from p in db.Project_Issue
                       where p.proj_surr_key == prid && p.IssueFileType.Equals("Image")
                       select p);
            return res.OrderByDescending(p => p.IssueID).FirstOrDefault().IssueFile; //res.OrderByDescending(p => p.IssueID)
        }


        public List<BuikIssueEntity> GetProjectIssueListTestScript()
        {
            List<BuikIssueEntity> res = new List<BuikIssueEntity>();
            for (int i = 0; i < 600; i++)
            {
                BuikIssueEntity r1 = new BuikIssueEntity();//{
                r1.IssueId = 999 + i;
                r1.IssuetypeId = 1;
                r1.IssueDetail = "TestIssueDetail for maximum issue list no " + i;
                r1.IssueDateTimeCreate = DateTime.Now.ToShortDateString();
                r1.IssueCreatorName = "Tess Script 01";
                r1.IssueCreatCompany = "Cenix Soft test1";
                r1.IssueComment = i;
                r1.IssueDeadline = DateTime.Now.ToShortDateString();
                r1.IssueFileType = "Text";
                r1.IssueProgress = i;
                r1.ProjectSurrKey = 99;
                r1.IssueLike = i;
                r1.IsPersonINC = 3;//,"","","","",2+ i,i, DateTime.Now.ToShortDateString(),"","text",i,"Cenix Soft",2013009
                res.Add(r1);
            };
            return res;
        }


        
        private int GetMaxIssueId()
        {
            var db = new BuilkChameleonEntities();
            return Convert.ToInt32(db.Project_Issue.Max(p => p.IssueID));
        }
        private Int32 AddProjectIssue(Int32 issuId,Int32 prid, int itype, string issuedetail, Int32 userid, string issuefile, string issueFloopPlan, string desline, int prog, string filetype)
        {
            var db = new BuilkChameleonEntities();
            Project_Issue pi = new Project_Issue();
            pi.IssueID = issuId;// (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            pi.IssueFloorPlanPhoto = issueFloopPlan;
            pi.IssueFile = issuefile;
            pi.IssueDeadline = desline;
            pi.IssueProgress = prog;

            string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile,IssueDeadline,IssueFileType) ";
            sql1 += "values(" + pi.IssueID + "," + pi.proj_surr_key + "," + pi.IssueTypeId + ",'" + pi.IssueDetail + "'," + pi.IssueCreator + ",'" + pi.IssueDateTimeCreate + "'," + pi.IssueProgress + ",'" + pi.IssueFloorPlanPhoto + "','" + pi.IssueFile + "','" + pi.IssueDeadline + "','" + filetype + "')";
            try { db.Database.ExecuteSqlCommand(sql1); }
            catch { return 0; }
            return Convert.ToInt32(pi.IssueID);
        }

        private Int32 UpdateProjectIssueDT(Int32 projectissueid, string datetime)
        {
            var db = new BuilkChameleonEntities();
            string sql1 ="update Project_Issue set IDateTimeCreate = '"+datetime+"' where IssueId = "+projectissueid+" ";
            try{db.Database.ExecuteSqlCommand(sql1);}
            catch{return 0;}
            return 1;
        }
        public int DumpDupProjectIssue()
        {
            var db = new BuilkChameleonEntities();
            int i = 0;
            foreach(var pr in db.Project_Issue)
            {
                //String.IsNullOrEmpty(p.IssueFloorPlanPhoto) ? "DefaultAva1.jpg" : p.IssueFloorPlanPhoto
                Project_Issue pi = new Project_Issue();
                pi.IssueID = Convert.ToInt32(pr.IssueID);
                pi.proj_surr_key = pr.proj_surr_key;
                pi.IssueID = Convert.ToInt32(pr.IssueID);
                pi.IssueCreator = pr.IssueCreator;
                pi.IssueDateTimeCreate = String.IsNullOrEmpty(pr.IssueDateTimeCreate) ? "" : pr.IssueDateTimeCreate;
                pi.IssueDeadline = String.IsNullOrEmpty(pr.IssueDeadline) ? "" : pr.IssueDeadline;
                pi.IssueDetail = String.IsNullOrEmpty(pr.IssueDetail) ? "" : pr.IssueDetail;
                pi.IssueFile = String.IsNullOrEmpty(pr.IssueFile) ? "" : pr.IssueFile;
                pi.IssueFileType = String.IsNullOrEmpty(pr.IssueFileType) ? "": pr.IssueFileType;
                pi.IssueFloorPlanPhoto = String.IsNullOrEmpty(pr.IssueFloorPlanPhoto) ? "":pr.IssueFloorPlanPhoto;
                pi.IssueProgress = pr.IssueProgress.HasValue ? pr.IssueProgress : 0;
                pi.IssueTypeId = pr.IssueTypeId;
                //AddProjectIssue(pi.IssueID,pi.proj_surr_key, pi.IssueTypeId, pi.IssueDetail,(int)pi.IssueCreator, pi.IssueFile, pi.IssueFloorPlanPhoto, pi.IssueDeadline,(int)pi.IssueProgress, pi.IssueFileType);
                
                i = i+UpdateProjectIssueDT(pi.IssueID, pr.IssueDateTimeCreate);
            } return i;
        }
        public int SetCollaborationShow(Int32 prid, Int32 userid)
        {
            int res = 0;
            try
            {
                string sql1 = "update Project_Collaboration set IsShow = " + 1 + " where user_surr_key = " + userid + " and proj_surr_key = " + prid + "";
                res = new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
                res = 1;
            }
            catch { res = 0; }
            return res;
        }

        public int SetCollaborationHide(Int32 prid, Int32 userid)
        {
            int res = 0;
            try
            {
                string sql1 = "update Project_Collaboration set IsShow = " + 0 + " where user_surr_key = " + userid + " and proj_surr_key = " + prid + "";
                res = new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
                res = 1;
            }
            catch { res = 0; }
            return res;
        }

        public Int32 GetProjectIdFromIssue(Int32 issueid)
        {
            Int32 res = 0;
            res = new BuilkChameleonEntities().Project_Issue.Where(p => p.IssueID == issueid).FirstOrDefault().proj_surr_key;
            return res;

        }

        // Knothing Added this 13/3/2014
        public string GetProjectCoverPhoto(Int32 prid)
        {
            string res = "";
            try
            {
                res = new BuilkChameleonEntities().T_PROJECT.Where(p => p.proj_surr_key == prid).FirstOrDefault().proj_photo_filename;
                return res;
            }
            catch { return ""; }
        }

        public List<IntStringObj> GetIssueLikeUser(Int32 IssueId)
        {
            //List<IntStringObj> res = new List<IntStringObj>();
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            var dd2 = (from p in db.IssueLikes
                       join t in db.T_USER
                       on p.LikeUserId equals t.user_surr_key into g
                       where p.IssueId == IssueId
                       from g1 in g
                       select new
                       {
                           Id = p.LikeUserId,
                           Name = g1.user_fullname
                       }).ToList().Select(rr => new IntStringObj
                       {
                           number = rr.Id,
                           msg = rr.Name
                       }
                          );
            return dd2.ToList<IntStringObj>();
        }

        public List<String> GetProjectCountryList()
        {
            //List<String> res = new List<string>();
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            var tt = db.T_PROJECT.Select(p => p.proj_country).Distinct();
            return tt.ToList<String>();
        }

        //GetProjectCollabarationList(int prid)
    }
}
