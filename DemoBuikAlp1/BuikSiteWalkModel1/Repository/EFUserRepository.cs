﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.ModelEntities;
using BuikSiteWalkModel1.EFDataAccess;
using BuikSiteWalkModel1.Abstract;
using System.Data.Objects.SqlClient;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;

namespace BuikSiteWalkModel1.Repository
{
    public class EFUserRepository : IUserRepository
    {
        string fpath = "https://builkchamaleonstorage.blob.core.windows.net/upload/avatar/";
        public BuikUserEntity GetUserEntitybyEmail(string email)
        {
            BuikUserEntity res = new BuikUserEntity();    
            using(var db = new BuilkChameleonEntities())
            {
               
                var query = (from t in db.T_USER
                             where t.user_email.Equals(email)
                             select new
                             {
                                 Id = t.user_surr_key,
                                 ema = t.user_email,
                                 fn = t.user_name,
                                 sn = t.user_surname,
                                 fulln = t.user_fullname,
                                 crty = t.user_country,
                                 pho = t.user_avatar_filename,
                                 uc = t.user_company,
                                 bt = t.user_company_typeid,
                                 aps =  t.create_date
                             }).ToList().Select(oo => new BuikUserEntity
                             {
                                 UserId = oo.Id.ToString(),
                                 UserEmail = oo.ema,
                                 UserFirstName = oo.fn,
                                 UserLastName = oo.sn,
                                 UserFullName = oo.fulln,
                                 UserCountry = oo.crty,
                                 userPhoto = fpath + oo.pho,
                                 UserCompamy = oo.uc,
                                 CreateSince = oo.aps.HasValue? oo.aps.Value.ToString("MMMM") + " / " + oo.aps.Value.Year.ToString() : "null",
                                 UserBusinessType = oo.bt.HasValue ? oo.bt.ToString() : "0"
                             });
                                
                return query.FirstOrDefault();
            } 
            
        }
        public BuikUserEntity GetUserEntitybyId(Int32 uid)
        {
            
            BuikUserEntity res = new BuikUserEntity();
            using (var db = new BuilkChameleonEntities())
            {
               var query = db.T_USER
                    .Where(p => p.user_surr_key.Equals(uid))
                    .ToList().AsParallel()
                    .Select(p => new BuikUserEntity
                    {
                        UserId = p.user_surr_key.ToString(),
                        UserEmail = p.user_email,
                        UserFirstName = p.user_name,
                        UserLastName = p.user_surname,
                        UserFullName = p.user_fullname,
                        UserCountry = p.user_country,
                        userPhoto = fpath + p.user_avatar_filename,
                        UserCompamy = p.user_company,
                        UserBusinessType = p.user_company_typeid.HasValue? p.user_company_typeid.ToString() : "0"
                    }
                    ).SingleOrDefault();
                return query;
               
            }
           

        }
        public int GetUserIdByEmail(string usermail)
        {
            try
            {
                var rr = new BuilkChameleonEntities().T_USER.Where(p => p.user_email.Equals(usermail)).FirstOrDefault();
                int res = rr.user_surr_key;
                return res;
            }
            catch { return 0; }
        }
        public List<IntStringObj> SearchUserName(string k)
        {
            using (var db = new BuilkChameleonEntities())
            {
                 var res = from b in db.T_USER.AsParallel()
                          where b.user_fullname.Contains(k)
                          select new IntStringObj { number = b.user_surr_key, msg = b.user_fullname };
                return res.ToList();
            }
        }
        public List<IntStringObj> SearchUserNameOrEmail(string k)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var res = from b in db.T_USER
                          where b.user_fullname.Contains(k) || b.user_email.Contains(k)
                          select new IntStringObj { number = b.user_surr_key, msg = b.user_fullname };
                return res.ToList();
            }
        }
        public List<IntStringObj> SearchUserByEmail(string k)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var res = from b in db.T_USER.AsParallel()
                          where b.user_email.Contains(k)
                          select new IntStringObj { number = b.user_surr_key, msg = b.user_fullname };
                return res.ToList();
            }
        }

        public T_USER GetUserBySurrogateKey(int id) {
            using (var db = new BuilkChameleonEntities())
            {
                return db.T_USER.FirstOrDefault(i => i.user_surr_key.Equals(id));
            }
        }

        private Int32 GetMaxUserSurrogateKey()
        {
            var d = new BuilkChameleonEntities();
            int i = d.T_USER.Max(p => p.user_surr_key);
            return i;
        }
        public Int32 AddNewRegisterUser(BuikUserEntity user)
        {
            T_USER t = new T_USER();
            t.user_country = user.UserCountry;

            t.user_name = user.UserFirstName;
            t.user_surname = user.UserLastName;
            t.user_email = user.UserEmail;
            t.create_date = DateTime.Now;
            t.user_isJoinResearchProgram = true;
            int i= GetMaxUserSurrogateKey() +1;
            t.user_surr_key = i;
            string sql1 = "Insert into T_USER (user_name,user_surname,user_email,user_country,user_isJoinResearchProgram,user_status,create_date) ";
            sql1 += "values('" + user.UserFirstName + "','" + user.UserLastName + "','" + user.UserEmail + "','" + user.UserCountry + "',1,2,'"+DateTime.Now.ToShortDateString()+"')";
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            db.Database.ExecuteSqlCommand(sql1);
            return GetMaxUserSurrogateKey();
        }
        public void AddUsertoDbbySql(BuikUserEntity user,int id)
        {
            string sql1 = "Insert into T_USER (user_surr_key,user_name,user_surname,user_email,user_country,user_isJoinResearchProgram) ";
            sql1 += "values("+id+",'"+user.UserFirstName+"','"+user.UserLastName+"','"+user.UserEmail+"','"+user.UserCountry+"',true)";
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            db.Database.ExecuteSqlCommand(sql1);
        }
        public int UpdateUserDetail(Int32 userid, string userName,string usersname, string companyname,string businesstype)
        {
            var n1 = userName; var n2 = usersname; var n3 = companyname; var n4 = businesstype; var n5 = userid;
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            string sql1 = @"update T_USER set user_name = {0}, user_surname = {1}, user_company = {2}, user_company_typeid = {3}  where user_surr_key = {4}";


            return db.Database.ExecuteSqlCommand(sql1, n1, n2, n3, n4, n5);
            
        }
        public int UpdateUserDetail(Int32 userid, string userName, string usersname, string companyname, int businesstype)
        {
            
            var n1 = userName; var n2 = usersname; var n3 = companyname; var n4 = businesstype;var n5 = userid;
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            string sql1 = @"update T_USER set user_name = {0}, user_surname = {1}, user_company = {2}, user_company_typeid = {3}  where user_surr_key = {4}";
            
           
            return db.Database.ExecuteSqlCommand(sql1,n1,n2,n3,n4,n5);

        }
        public int UpdateUserName(Int32 userid, string userName, string usersname)
        {
            string sql1 = "update T_USER set user_name = '" + userName + "', user_surname = '" + usersname + "' where user_surr_key = '" + userid + "'";
            sql1 += "";
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            return db.Database.ExecuteSqlCommand(sql1);

        }
        public int UpdateuserPhoto(string filepath, Int32 userid)
        {
            int res = 0;
            string sql1 = "update T_USER set user_avatar_filename = '" + filepath + "' where user_surr_key = " + userid + "";
            try
            {
                BuilkChameleonEntities db = new BuilkChameleonEntities();
                res = db.Database.ExecuteSqlCommand(sql1);
            }
            catch { }
            return res;
        }

        private Int32 GetMaxInvitationId()
        {
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            Int32 max = db.T_INVITES.Select(p => p.invite_id).Max();
            return max;
        }

        public int AddUserInvitation(string email, Int32 uid, Int32 prid,string tokenid)
        {
            string sql1 = "insert into T_INVITES (email_invited,token_id,user_surr_key,project_surr_key,invitedate,invitation_status)";
            sql1 += "values('" +email+"','"+tokenid+"',"+uid+","+prid+",'"+DateTimeOffset.Now+"',0)";
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            if (db.Database.ExecuteSqlCommand(sql1) == 1) { return GetMaxInvitationId(); }
            else return 0;
        }

        public int UpdateUserInvitation(Int32 invtid)
        { 
            string sql1 = "Update T_INVITES set invitation_status = 1, registerdate = '"+ DateTime.Now.ToShortDateString() +"' where invite_id = "+invtid+"";
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            return db.Database.ExecuteSqlCommand(sql1);
        }

        public void ScanAndUpdateInvitation(string useremail,Int32 userid)
        {
            EFProjectRepository prepo = new EFProjectRepository();
            BuilkChameleonEntities db = new BuilkChameleonEntities();
            //var tt = from T_INVITES t in db.T_INVITES
            //         where t.email_invited.Equals(useremail) && t.invitation_status.Equals('0')
            //         select t;
            IEnumerable<T_INVITES> tt = db.T_INVITES.Where(p => p.email_invited.Equals(useremail) && p.invitation_status.Equals("0"));
            foreach(var gg in tt)
            {
                UpdateUserInvitation(gg.invite_id);
                prepo.AddProjectCollaborationWithinvitedId((Int32)gg.project_surr_key, userid,(Int32)gg.user_surr_key, 2);
            }
        }

        public Int32 SearchUserEmail(string email)
        {
            Int32 id;
            try
            {
                id = new BuilkChameleonEntities().T_USER.Where(u => u.user_email.Equals(email)).FirstOrDefault().user_surr_key;
                return id;
            }
            catch{return 0;}
        }
    }
}
