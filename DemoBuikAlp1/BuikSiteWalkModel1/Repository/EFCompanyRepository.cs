﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.EFDataAccess;

namespace BuikSiteWalkModel1.Repository
{
    public class EFCompanyRepository : BuikSiteWalkModel1.Abstract.ICompanyRepository
    {
        public T_COMPANY GetCompanyById(int companyId)
        {
            using (var db = new BuilkChameleonEntities())
            {
                var query = db.T_COMPANY.Where(i => i.company_surr_key == companyId).FirstOrDefault();


                return query;
            }

        }
    }
}
