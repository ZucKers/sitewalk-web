﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.EFDataAccess;
using BuikSiteWalkModel1.ModelEntities;

namespace BuikSiteWalkModel1.Repository
{
    public class EFIssueRepository
    {
        BuilkChameleonEntities db = new BuilkChameleonEntities();

        private int GetMaxIssueCommentId()
        {
            int i = 1;
            try { i =  (int)db.IssueComments.Max(p => p.IssueCommentId)+1;}
            catch{}
            return i;
        }

        private int GetMaxIssueId()
        {
            return Convert.ToInt32(db.Project_Issue.Max(p => p.IssueID));
        }

        public List<IntStringObj> GetIssueType()
        {
           var res = from IssueType in db.IssueTypes
                               select new IntStringObj
                               {
                                   number = IssueType.IssueTypeId,
                                   msg = IssueType.IssueTypeName
                               };
            return res.ToList();
        }

        public List<IntStringObj> GetUserIssueInvolve(int Issueid)
        {
            var res = from p1 in db.IssuedPersonInChargeds where p1.IssueId.Equals(Issueid)
                      join p2 in db.T_USER on p1.user_surr_key equals p2.user_surr_key
                      
                      select new IntStringObj
                        {
                            number = p2.user_surr_key,
                            msg = p2.user_fullname
                        };
            return res.ToList() ;
        }

        public Int32 AddProjIssueComment(int userid, int issueid, string issuecomment)
        {
            IssueComment i = new IssueComment();
            i.IssueCommentDateTime = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            i.IssueCommentDetail = issuecomment;
            i.IssueCommentUserId = userid;
            i.IssueCommentId = GetMaxIssueCommentId();
            i.IssueId = issueid;
           string sql1 = "Insert into IssueComment (IssueCommentUserId,IssueCommentDetail,IssueCommentDateTime,IssueId)";
            sql1 += " values(" + i.IssueCommentUserId + ",'" + i.IssueCommentDetail + "','" + i.IssueCommentDateTime + "'," + i.IssueId + ")";
            try
            {
                db.Database.ExecuteSqlCommand(sql1);
                return GetMaxIssueCommentId();
            }
            catch { return 0; }
            
        }

        public int AddLikeIssue(Int32 issueid,Int32 userid)
        {
            try
            {
                IssueLike i = new IssueLike();
                i.IssueId = issueid; i.LikeUserId = userid;
                i.LikeDateTime = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
                string sql1 = "Insert into IssueLike (IssueId,LikeUserId,LikeDatetime)";
                sql1 += " values (" + i.IssueId + "," + i.LikeUserId + ",'" + i.LikeDateTime + "')";
                return db.Database.ExecuteSqlCommand(sql1);
            }
            catch { return 0; }
           
        }

        public int RemoveLikeIssue(Int32 issueid, Int32 userid)
        {
            IssueLike i = new IssueLike();
            i.IssueId = issueid; i.LikeUserId = userid;
            i.LikeDateTime = DateTime.Now.ToShortDateString();
            string sql1 = "delete from IssueLike where IssueId = "+issueid+" and LikeUserId = "+userid+";//(IssueId,LikeUserId,LikeDatetime)";
            return db.Database.ExecuteSqlCommand(sql1);
           
        }     

        private int AddIssueRequestForInformation(int prid,int itype,string issuedetail,int userid) 
        {
            Project_Issue pi = new Project_Issue();
            pi.IssueID = (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
           string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile) ";
            sql1 += "values('"+pi.IssueID+"',"+pi.proj_surr_key+","+pi.IssueTypeId+",'"+pi.IssueDetail+"',"+pi.IssueCreator+",'"+pi.IssueDateTimeCreate+"',"+pi.IssueProgress+",'"+pi.IssueFloorPlanPhoto+"','"+pi.IssueFile+"')";
            return db.Database.ExecuteSqlCommand(sql1);
            
        }
        private int AddIssueGoodJob(int prid, int itype, string issuedetail, int userid) 
        {
            Project_Issue pi = new Project_Issue();
            pi.IssueID = (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile) ";
            sql1 += "values('" + pi.IssueID + "'," + pi.proj_surr_key + "," + pi.IssueTypeId + ",'" + pi.IssueDetail + "'," + pi.IssueCreator + ",'" + pi.IssueDateTimeCreate + "',0,'" + pi.IssueFloorPlanPhoto + "','" + pi.IssueFile + "')";
            return db.Database.ExecuteSqlCommand(sql1);
            
        }
        private int AddIssueProgress(int prid, int itype, int percentprogress, int userid)
        {
            Project_Issue pi = new Project_Issue();
            pi.IssueID = (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueProgress = percentprogress;
            //pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile) ";
            sql1 += "values('" + pi.IssueID + "'," + pi.proj_surr_key + "," + pi.IssueTypeId + ",'"+pi.IssueProgress.Value.ToString()+"'," + pi.IssueCreator + ",'" + pi.IssueDateTimeCreate + "'," + pi.IssueProgress + ",'" + pi.IssueFloorPlanPhoto + "','" + pi.IssueFile + "')";
            return db.Database.ExecuteSqlCommand(sql1);
            
        }
        private int AddissueActionRequired(int prid, int itype, string issuedetail, int userid,List<int> personinchargedid,string deadlined) 
        {
            Project_Issue pi = new Project_Issue();
            pi.IssueID = (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile) ";
            sql1 += "values('" + pi.IssueID + "'," + pi.proj_surr_key + "," + pi.IssueTypeId + ",'" + pi.IssueDetail + "'," + pi.IssueCreator + ",'" + pi.IssueDateTimeCreate + "'," + pi.IssueProgress + ",'" + pi.IssueFloorPlanPhoto + "','" + pi.IssueFile + "')";
            db.Database.ExecuteSqlCommand(sql1);
                     
            foreach(int i in personinchargedid)
            {
                this.AddPersonInChargedIssue(Convert.ToInt32(pi.IssueID), i, 0, deadlined);
            }
            return db.SaveChanges(); 
        }

        public int AddPersonInChargedIssue(Int32 issueid,Int32 userid,int issuestatus,string deadline)
        {
            IssuedPersonInCharged ipc = new IssuedPersonInCharged();
            ipc.IssueId = issueid;
            ipc.user_surr_key = userid;
            ipc.IssueDeadLineDate = deadline;
            ipc.IssuedStatus = issuestatus;
            string sql1 = "insert into IssuedPersonInCharged (IssueId,user_surr_key,user_name,IssuedStatus,IssueDeadLineDate) ";
            sql1 +="values ("+ipc.IssueId+","+ipc.user_surr_key+",'"+ipc.user_name+"',"+ipc.IssuedStatus+",'"+ipc.IssueDeadLineDate+"')";
            return db.Database.ExecuteSqlCommand(sql1);
            
        }

        public Int32 AddProjectIssue(Int32 prid, int itype, string issuedetail, Int32 userid, string issuefile, string issueFloopPlan,string desline,int prog,string filetype)
        {

            Project_Issue pi = new Project_Issue();
            pi.IssueID = (GetMaxIssueId() + 1);
            pi.proj_surr_key = prid;
            pi.IssueTypeId = itype;
            pi.IssueDetail = issuedetail;
            pi.IssueCreator = userid;
            pi.IssueDateTimeCreate = DateTime.Now.ToString("G", CultureInfo.CreateSpecificCulture("en-US"));
            pi.IssueFloorPlanPhoto = issueFloopPlan;
            pi.IssueFile = issuefile;
            pi.IssueDeadline = desline;
            pi.IssueProgress = prog;

            string sql1 = "insert into Project_Issue (IssueId,proj_surr_key,IssueTypeId,IssueDetail,IssueCreator,IssueDateTimeCreate,IssueProgress,IssueFloorPlanPhoto,IssueFile,IssueDeadline,IssueFileType,IDateTimeCreate) ";
            sql1 += "values(" + Convert.ToInt32(pi.IssueID) + "," + pi.proj_surr_key + "," + pi.IssueTypeId + ",'" + pi.IssueDetail + "'," + pi.IssueCreator + ",'" + pi.IssueDateTimeCreate + "'," + pi.IssueProgress + ",'" + pi.IssueFloorPlanPhoto + "','" + pi.IssueFile + "','" + pi.IssueDeadline + "','" + filetype + "','" + pi.IssueDateTimeCreate + "')";
            try { db.Database.ExecuteSqlCommand(sql1); }
            catch { return 0; }
            return Convert.ToInt32(pi.IssueID);
        }

        public int UpdateProjectIssuePhoto(string issueid, string filepat)
        {
            string sql1 = "update Project_Issue set IssueFile = '"+filepat+"' where IssueId = '"+issueid+"' ";
            int i = 0;
            try {i =  db.Database.ExecuteSqlCommand(sql1); }
            catch { return 0; }
            return i;
        }

        public int UpdateProjectIssueFloorPlan(string issueid, string filepat)
        {
            Int32 isid = Convert.ToInt32(issueid);
            string sql1 = "update Project_Issue set IssueFloorPlanPhoto = '" + filepat + "' where IssueId = '" + isid + "' ";
            int i = 0;
            try { i = db.Database.ExecuteSqlCommand(sql1); }
            catch { return 0; }
            return i;
        }

        public List<BuikIssueEntity> GetProjectIssue(Int32 prid)
        {
            List<BuikIssueEntity> res = new List<BuikIssueEntity>();
            var r = db.Project_Issue.Where(p => p.proj_surr_key == prid);
            return res;
        }

        private int RemoveAllLike(Int32 isid)
        {
            try
            {
                string sql1 = "delete from IssueLike where IssueId = " + isid + " ";// and LikeUserId = " + userid + ";//(IssueId,LikeUserId,LikeDatetime)";
                return db.Database.ExecuteSqlCommand(sql1);
            }
            catch { return 0; }
           
        }
        private int RemoveAllCommnent(Int32 isid)
        {
            try
            {
                string sql1 = "delete from IssueComment where IssueId = " + isid + " ";// and LikeUserId = " + userid + ";//(IssueId,LikeUserId,LikeDatetime)";
                //sql1 += " values (" + i.IssueId + "," + i.LikeUserId + ",'" + i.LikeDateTime + "')";
                return db.Database.ExecuteSqlCommand(sql1);
            }
            catch { return 0; }
        }
        public int RemoveProjectIssue(string issueid)
        {
            Int32 isid = Convert.ToInt32(issueid);
            int res = 0;
            try{
                string sql1 = "delete from Project_Issue where IssueId = "+isid+"";               
                res =  new BuilkChameleonEntities().Database.ExecuteSqlCommand(sql1);
                RemoveAllCommnent(Convert.ToInt32(issueid));
                RemoveAllLike(Convert.ToInt32(issueid));
            }
            catch{return 0;}
            return res;
        }

        public Int32 GetProjectIssueCreator(string isseid)
        {
            //Int32 res = new BuilkChameleonEntities().Project_Issue.Where(p => p.IssueID.Equals(Convert.ToInt32(isseid))).Select(q => q.IssueCreator).SingleOrDefault();
            var db = new BuilkChameleonEntities();
            Int32 rrr = Convert.ToInt32(isseid);
            Int32 res2 = (Int32)db.Project_Issue.Where(p => p.IssueID == rrr).Select(q => q.IssueCreator).SingleOrDefault();
            return res2;
        }

        public string GetIssuePhotoFile(Int32 issueid)
        {
            var db = new BuilkChameleonEntities();
            string res = db.Project_Issue.Where(p => p.IssueID == issueid).Select(q => q.IssueFile).SingleOrDefault();
            return res;
        }
    }
}