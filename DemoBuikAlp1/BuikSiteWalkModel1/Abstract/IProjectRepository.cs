﻿using System;
using BuikSiteWalkModel1.ModelEntities;
using System.Collections.Generic;
namespace BuikSiteWalkModel1.Abstract
{
    public interface IProjectRepository
    {
        string AddNewProjectMobile(BuikSiteWalkModel1.ModelEntities.BuikProjectEntity pr);
        string AddProjectCollaboration(BuikSiteWalkModel1.ModelEntities.BuikProjectCollaborationEntity pc);
        void AddProjectCollaboration(int prid, int userid, int sta);
        void AddProjectCollaboration(string prid, string userid);
        void AddProjectFloorPlan(string prid, string floorplanfilename);
        void EditProjectMobile(BuikSiteWalkModel1.ModelEntities.BuikProjectEntity pr1);
        System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.BuikProjectTypeEntity> GetAllProjectType();
        System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.BuikIssueComment> GetIssueCommentDetail(int issueids);
        Int32 GetMaxProjSurrogateKey();
        List<BuikProjectEntity> GetProjectMobileEntityByCollaborationIdEmail(string useremail);
        List<ProjectDetailMobile> GetProjectDetailMobileByEmail(string email);
        //List<BuikProjectEntity> GetProjectMobileEntityByCollaborationIdEmail(string useremail);
        List<ProjectDetailMobile> GetProjectDetailMobile(int id);
        //System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.ProjectDetailMobile> GetProjectDetailMobile(string email);
        List<BuikIssueEntity> GetProjectIssueList(int prid);
        List<BuikProjectEntity> GetProjectMobileEntityByCollaborationId(int id);
        //System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.BuikProjectEntity> GetProjectMobileEntityByCollaborationId(string useremail);
        
        List<BuikProjectEntity> GetProjectMobileEntityByUser(int id);
        List<BuikProjectTypeEntity> GetProjectTypeBCulture(string cid);
        Int32 GetUserIdByEmail(string usermail);
        Int32 GetUserIdByName(string username);
        int IsProjecttIsFav(int prid, int invitedid);
        int RemoveProjectCollaboration(Int32 prid, Int32 userid, Int32 inviter);
        int RemoveProjectCollaboration(Int32 prid, Int32 userid);
        System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.BuikProjectEntity> SortedByFav(System.Collections.Generic.List<BuikSiteWalkModel1.ModelEntities.BuikProjectEntity> prList, int userid);
        int UpdateProjectCollaboration(string prid, string userid, string inviter, int stats);
        List<IntStringObj> GetProjectFloorPlan(Int32 prid);
        string UpdateProjectProfilePhoto(Int32 prid, string filelocation);
        string AddProjectFloorPlanPhoto(Int32 prid, string filelocation,string fpname);
        List<BuikProjectCollaborationEntity> GetProjectCollaborationRequest(Int32 userid);
        int SetProjAsFav(Int32 userid, Int32 prid,int s);
        Int32 GetProjColInviter(Int32 prid, Int32 inviteduser);
        int RemoveLike(Int32 issueid, Int32 userid);
        List<IntStringObj> GetProjectListForUser(Int32 userid);
        List<ProjColList> GetProjectCollabarationList(Int32 prid);
        int AddNotification(string fuid, string tuid, int isR, string msg, string nf, string did);
        int UpdateNotifIsRead(Int32 ID, int val);
        List<NotificationEntity> GetNotificationById(string uid);
        List<BuikIssueEntity> GetProjectIssueListByIssueId(string issueid);
        List<BuikIssueComment> GetIssueCommentDetail2(Int32 issueids);
        ProjectDetailMobile GetProjectDetailByPrid(Int32 prid);
        List<IntStringObj> GetCompanyNamebyId(Int32 uid);
        int UpdateProjectCompany(Int32 prid, Int32 cid);
        string GetCompanyByProjectId(Int32 prid);
        int RemoveFPPhoto(string filepath);
        int EditProjectName(Int32 prid, string name);
        int EditProjCustomer(Int32 prid, string cname);
        int EditProjectCountry(Int32 prid,string c);
        int EditProjectProvince(Int32 prid, string pr);
        int EditProjectStartDate(Int32 prid, string d);
        int EditProjectEndDate(Int32 prid, string d);
        string GetProjectComapanyName(Int32 prid);
        string GetVersion();
        int SetVersion(string f);
        string GetLatestIssuePhoto(Int32 prid);
        string AddNewProjectMobileWithLat(BuikProjectEntity pr);
        int RemoveIssueComment(Int32 icid);
       // void PumpDataToProjectColl();
        List<BuikIssueEntity> GetProjectIssueListTestScript();
        string AddProjectCollaborationWithinvitedId(Int32 prid, Int32 userid, Int32 inviter, int sta);
        int AddProjectCollaboration(Int32 prid, Int32 userid, int sta, int isfav,Int32 inviter);
        int UpdateProjectCollaboration2(string prid, string userid, string inviter, int stats);
        List<ProjColList> GetProjectEmailInvitedCollaboration(Int32 prid, Int32 userid);
        int RemoveEmailInvitation(string email, Int32 prid, Int32 userid);
        int SetCollaborationShow(Int32 prid, Int32 userid);
        int SetCollaborationHide(Int32 prid, Int32 userid);
        Int32 GetProjectCreator(Int32 prid);
        List<BuikProjectEntity> GetProjectMobileEntityByCollaborationIdShowAll(Int32 id);
        List<BuikIssueEntity> GetProjectIssueList(Int32 prid, int PageSize, int page);
        Int32 GetProjectIdFromIssue(Int32 issueid);

        // Knothing Added this 13/3/2014
        string GetProjectCoverPhoto(Int32 prid);
        List<String> GetProjectCountryList();
    }
}
