﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BuikSiteWalkModel1.EFDataAccess;

namespace BuikSiteWalkModel1.Abstract
{
    public interface ICompanyRepository
    {
        T_COMPANY GetCompanyById(int companyId);
    }
}
